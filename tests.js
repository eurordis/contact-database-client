const { getvalue, createvalue } = require('./src/js/utils')


const testing = {
  equal(a, b, message = '') {
    if(JSON.stringify(a) === JSON.stringify(b)) console.info('✅', message || 'Passed')
    else console.error('❌', message || 'Error', '\n\t↪ Parts are not equal:\n\t', a, '\n\t ≠\n\t', b)
  }
}

testing.equal(getvalue('hey'), { value: 'hey', status: '', modifier: '' })
testing.equal(getvalue('<hey'), {  value: 'hey', status: '<', modifier: '' })
testing.equal(getvalue('-hey'), {  value: 'hey', status: '', modifier: '-' })
testing.equal(getvalue('[hey]'), { value: '[hey]', status: '', modifier: '' })
testing.equal(getvalue('<+[hey]'), {  value: '[hey]', status: '<', modifier: '+' })

testing.equal(createvalue('*+[hey]'), '*+[hey]')
testing.equal(createvalue('[hey]', '<', '*'), '*<[hey]')
testing.equal(createvalue('*+[hey]', '<', '*'), '*<[hey]')
testing.equal(createvalue('*+[hey]', '<', ''), '<[hey]')
testing.equal(createvalue('*+[hey]', '', '*'), '*[hey]')
