riot.tag2('activity', '<h1>Recent Browsing</h1> <div class="row"> <button onclick="{this.dumpActivity}">Export activity as CSV</button> <button onclick="{this.dumpSearchActivity}">Export search activity as CSV</button> </div> <ul> <li each="{activity in this.opts.activities}"> <time>{trans(new Date(activity.at * 1000))}</time> — <a href="/person/{activity.by.pk}">{activity.by.label}</a>: <a href="{activity.location}">{activity.location}</a> </li> </ul>', 'activity time,[data-is="activity"] time{ font-weight: bold; font-size: .6rem; }', '', function(opts) {
    this.dumpActivity = async () => {
      downloadUrl('/activity.csv', 'activity.csv')
    }

    this.dumpSearchActivity = async () => {
      downloadUrl('/search-activity.csv', 'search-activity.csv')
    }
});

riot.tag2('add-export', '<virtual if="{this.opts.label}"> <button if="{!this.inList}" class="to-add" title="Add for export" onclick="{this.addToExport}"> <icon figure="basket-small"></icon>add: {this.opts.label} </button> <button if="{this.inList}" title="Remove from export" onclick="{this.removeFromExport}"> <icon figure="basket-small"></icon>added: {this.opts.label} </button> </virtual> <virtual if="{!this.opts.label}"> <icon if="{!this.inList}" figure="basket" onclick="{this.addToExport}" title="Add for export" class="to-add"></icon> <icon if="{this.inList}" figure="basket" onclick="{this.removeFromExport}" title="Remove from export"></icon> </virtual>', 'add-export icon,[data-is="add-export"] icon{ width: 1.2rem; height: 1.2rem; cursor: pointer; } add-export icon.to-add,[data-is="add-export"] icon.to-add{ opacity: .4; } add-export button,[data-is="add-export"] button{ display: inline-flex; } add-export button:not(.to-add),[data-is="add-export"] button:not(.to-add){ background-color: #e0eaf3; }', '', function(opts) {
    this.on('*', () => {
      const organisations = exportList.all().map(l => l.organisation)
      if(this.opts.organisationPk && this.opts.personPk) {
        const contact = exportList.all().find(l => (l.organisation === this.opts.organisationPk && l.person === this.opts.personPk))
        this.inList = Boolean(contact)
      }
      else if(this.opts.organisationPk) this.inList = organisations.includes(this.opts.organisationPk)
      else if(this.opts.personPk) {
        const persons = exportList.all().filter(l => !l.organisation).map(l => l.person)
        this.inList = persons.includes(this.opts.personPk)
      }
    })

    this.addToExport = () => {
      if(this.opts.personPk && this.opts.organisationPk) exportList.add({ person: this.opts.personPk, organisation: this.opts.organisationPk })
      else if(this.opts.personPk) exportList.add({ person: this.opts.personPk })
      else if(this.opts.roles && this.opts.roles.length) exportList.addMany(this.opts.roles)
      else exportList.add({ organisation: this.opts.organisationPk })
      updatePage()
    }

    this.removeFromExport = () => {
      if(this.opts.personPk && this.opts.organisationPk) exportList.remove({ person: this.opts.personPk, organisation: this.opts.organisationPk })
      else if(this.opts.organisationPk) exportList.removeOrganisation(this.opts.organisationPk)
      else if(this.opts.personPk) exportList.remove({ person: this.opts.personPk })
      updatePage()
    }
});

riot.tag2('admin', '<section> <h1>Admin Actions</h1> <blockquote> Release version {this.config.VERSION} </blockquote> <button if="{user.can(\'import\')}" onclick="{this.toggleGhostMode}"> <span if="{!this.ghostMode}">Enable</span> <span if="{this.ghostMode}">Disable</span> &nbsp;Ninja Mode </button> <ul> <li><a href="/history">View logs of the database</a></li> <li><a href="#" onclick="{this.getHistoryDump}">Export logs of the database</a></li> <li><a href="#" onclick="{this.getEventsDump}">Export all events</a></li> <li><a href="#" onclick="{this.getGroupsDump}">Export all groups</a></li> <li><a href="#" onclick="{this.getKeywordsDump}">Export all keywords</a></li> <li><a href="#" onclick="{this.getDiseasesDump}">Export all diseases</a></li> <li><a href="#" onclick="{this.getRepresentativesStats}">Export ePAG representatives statistics</a></li> <li><a href="/reports.zip" target="_blank">Export the full report (nightly updated)</a></li> </ul> <div if="{user.can(\'import\')}"> <h3>Super Users Actions</h3> <ul> <li><a href="#" onclick="{this.getPersonsDump}">Export all persons</a></li> <li><a href="#" onclick="{this.getOrganisationsDump}">Export all organisations</a></li> <li><a href="#" onclick="{this.getRolesDump}">Export all Contact/Organisation roles</a><tooltip description="Emails for this export are not interpreted. If the contact/organisation role does not have any email, the email section will be empty." link="https://gitlab.com/eurordis/contact-database/-/wikis/Exports-handbook"></tooltip></li> <li><a href="#" onclick="{this.getRolesGroups}">Export all group roles</a></li> <li><a href="#" onclick="{this.getSpeakersDump}">Export all attendees</a></li> </ul> </div> </section> <section> <h2>Membership Actions</h2> <div class="row"> <select class="w33p" name="membership" onchange="{this.updateMembershipSelectField}"> <option value="eurordis">EURORDIS</option> <option value="rdi">RDI</option> </select> </div> <ul> <li if="{user.can(\'import\')}"><a href="#" onclick="{this.getMembersDump}">Export all members</a></li> <li if="{user.can(\'import\')}"><a href="#" onclick="{this.getWithdrawnMembers}">Export withdrawn members</a></li> <li><a href="#" onclick="{this.getMembersStats}">Export members statistics</a></li> <li if="{user.can(\'import\')}"><a href="#" onclick="{this.getWebsiteMembers}">Export members for website</a></li> <li if="{user.can(\'import\')}" hidden="{!this.displayVotingContact}"><a href="#" onclick="{this.getFullMembersWithoutVoting}">Export full members without voting contact</a></li> <li if="{user.can(\'import\')}" hidden="{!this.displayVotingContact}"><a href="#" onclick="{this.getVotingContactsDump}">Export all voting contacts</a></li> </ul> </section> <section> <h2>Fees Reports</h2> <p class="row"> Get the report of fee for year <input onchange="{this.updateYear}" riot-value="{this.year}" type="{\'number\'}"><button onclick="{this.getFeesLink}">Download</button> </p> </section> <section if="{user.can(\'import\')}"> <h2>Grouping comparison</h2> <div class="row"> <input type="file" ref="grouping-file"><button onclick="{this.compareGroupings}">Compare</button> </div> <small>A txt file with groupings is expected. See the <a href="https://gitlab.com/eurordis/contact-database/uploads/c3f23471f0022691073fe7b8bd61650a/Grouping.txt" target="_blank">example file</a>.</small> </section> <section if="{user.can(\'import\')}"> <h2>Import contacts</h2> <div class="row"> <input type="file" ref="file"><button onclick="{this.upload}">Import</button> </div> <div if="{this.errors.length}" class="import-error"> <h3 class="error"> <icon figure="message-alert-small"></icon> Errors: {this.errorSummary} </h3> <ul> <li each="{error in this.errors}"> <a href="{error.data.uri + location.search}">{error.raw.first_name} {error.raw.last_name} - {error.raw.email             || \'Empty email\'}</a>: <ul> <li each="{msg, key in error.report}">{key}: {msg}</li> </ul> </li> </ul> </div> <bulk-warning warnings="{this.warnings}"></bulk-warning> </section> <details if="{user.can(\'delete\')}"> <summary><h2>Manage Groups</h2></summary> <form class="row" each="{group in this.groups}" onsubmit="{this.saveGroup}" pk="{group.pk}"> <input class="w33p" name="name" riot-value="{group.name}" placeholder="Name" required> <input class="w33p" name="description" riot-value="{group.description}" placeholder="Description"> <select class="w33p" name="kind"> <option value=""></option> <option each="{kind in this.kinds}" riot-value="{kind.key}" selected="{group.kind == kind.key}">{kind.value}</option> </select> <button>Save</button> <button type="button" class="danger" onclick="{this.deleteGroup}" pk="{group.pk}" if="{group.pk}">Delete</button> </form> </details> <details if="{user.can(\'delete\')}"> <summary><h2>Manage Keywords</h2></summary> <form class="row" each="{keyword in this.keywords}" onsubmit="{this.saveKeyword}" pk="{keyword.pk}"> <input name="name" riot-value="{keyword.name}" placeholder="Name" required> <select name="kind"> <option value=""></option> <option each="{kind in this.config.KEYWORD_KIND}" riot-value="{kind.key}" selected="{keyword.kind == kind.key}">{kind.value}</option> </select> <input type="checkbox" name="for_person" value="1" id="kw_for_person_{keyword.pk}" checked="{keyword.for_person}"><label for="kw_for_person_{keyword.pk}">Contact keyword</label> <input type="checkbox" name="for_organisation" value="1" id="kw_for_organisation_{keyword.pk}" checked="{keyword.for_organisation}"><label for="kw_for_organisation_{keyword.pk}">Organisation keyword</label> <button>Save</button> <button type="button" class="danger" onclick="{this.deleteKeyword}" pk="{keyword.pk}" if="{keyword.pk}">Delete</button> </form> </details>', 'admin input[type=number],[data-is="admin"] input[type=number]{ width: 100px; } admin input,[data-is="admin"] input{ max-width: 40%; } admin section,[data-is="admin"] section,admin details,[data-is="admin"] details{ margin-bottom: 2rem; } admin blockquote,[data-is="admin"] blockquote{ padding: 0; font-style: italic; font-size: .9em; } admin details,[data-is="admin"] details{ margin-top: 2rem; } admin details form,[data-is="admin"] details form{ max-width: inherit; } admin details h2,[data-is="admin"] details h2{ display: inline-block; }', '', function(opts) {
    this.year = (new Date()).getFullYear()
    this.config = config
    this.errors = []
    this.displayVotingContact = true

    this.on('mount', async () => {
      await this.loadGroups()
      await this.loadKeywords()
      this.kinds = this.config.GROUP_KIND.sort((a, b) => a.value.toLowerCase() >= b.value.toLowerCase() ? 1 : -1)
      this.update()
    })

    this.on('*', () => {
      this.ghostMode = Boolean(localStorage.ghostMode)
    })

    this.toggleGhostMode = () => {
      const ghostMode = !localStorage.getItem('ghostMode')
      ghostMode ? localStorage.setItem("ghostMode", "on") : localStorage.removeItem("ghostMode")
      window.location.reload()
    }

    this.updateYear = (event) => {
      this.year = event.target.value
    }

    this.loadGroups = async () => {
      const response = await api('/group')
      this.groups = config.GROUPS = response.data.data
      this.groups.push({})
    }

    this.saveGroup = async (event) => {
      event.preventDefault()
      const form = event.target
      const pk = form.getAttribute('pk')
      const data = serializeForm(form, true)
      let response = {}
      if(pk) response = await api(`/group/${pk}`, { method: 'put', data })
      else response = apiResponse(await api(`/group`, { data }))
      if(response.ok) notify('Group saved!', 'success')
      else if (this.ghostMode) notify("You're a ninja, you can't do that!", 'error')
      else notify(response.data.error, 'error')
      await this.loadGroups()
      this.update()
    }

    this.deleteGroup = async () => {
      if(!confirm('Are you sure you want to permanently delete this group?')) return
      const pk = event.currentTarget.getAttribute('pk')
      const response = await api(`/group/${pk}`, { method: 'delete' })
      if (response.ok) notify('Group deleted!', 'success')
      else notify(response.data.error, 'error')
      await this.loadGroups()
      this.update()
    }

    this.loadKeywords = async () => {
      const response = await api('/keyword')
      this.keywords = config.KEYWORDS = response.data.data
      this.keywords.push({})
    }

    this.saveKeyword = async (event) => {
      event.preventDefault()
      const form = event.target
      const pk = form.getAttribute('pk')
      const data = serializeForm(form, true)
      let response = {}
      if(pk) response = await api(`/keyword/${pk}`, { method: 'put', data })
      else response = await api(`/keyword`, { data })
      if(response.ok) notify('Keyword saved!', 'success')
      else if (this.ghostMode) notify("You're a ninja, you can't do that!", 'error')
      else notify(JSON.stringify(response.data), 'error')
      await this.loadKeywords()
      this.update()
    }

    this.deleteKeyword = async () => {
      if(!confirm('Are you sure you want to permanently delete this keyword?')) return
      const pk = event.currentTarget.getAttribute('pk')
      const response = await api(`/keyword/${pk}`, { method: 'delete' })
      if (response.ok) notify('Keyword deleted!', 'success')
      else notify(response.data.error, 'error')
      await this.loadKeywords()
      this.update()
    }

    this.updateMembershipSelectField = (event) => {
      this.displayVotingContact = event.target.value === 'eurordis'
      this.update()
    }

    this.getFeesLink = async () => {
      downloadUrl(`/report/fees?year=${this.year}`, `fees-${this.year}.xlsx`)
    }

    this.getMembersDump = async () => {
      const membershipName = document.getElementsByName("membership")[0].value
      downloadUrl(`/report/${membershipName}-members-status.xlsx`, `contact-database-${membershipName}-members-status.xlsx`)
    }

    this.getWithdrawnMembers = async () => {
      const membershipName = document.getElementsByName("membership")[0].value
      downloadUrl(`/report/withdrawn-${membershipName}-members.xlsx`, `contact-database-withdrawn-${membershipName}-members.xlsx`)
    }

    this.getFullMembersWithoutVoting = async () => {
      const membershipName = document.getElementsByName("membership")[0].value
      downloadUrl(`/report/full-${membershipName}-members-without-voting.xlsx`, `contact-database-full-${membershipName}-members-without-voting.xlsx`)
    }

    this.getSpeakersDump = async () => {
      downloadUrl('/report/all-attendees.xlsx', 'contact-database-all-speakers.xlsx')
    }

    this.getVotingContactsDump = async () => {
      downloadUrl('/report/all-voting-contacts.xlsx', 'contact-database-all-voting-contacts.xlsx')
    }

    this.getHistoryDump = async () => {
      downloadUrl('/history/export', 'contact-database-history.csv')
    }

    this.getPersonsDump = async () => {
      downloadUrl('/dump/person', 'contact-database-individuals.xlsx')
    }

    this.getRolesDump = async () => {
      downloadUrl('/dump/role', 'contact-database-roles.xlsx')
    }

    this.getRolesGroups = async () => {
      downloadUrl('/report/group-roles.xlsx', 'contact-database-group-roles.xlsx')
    }

    this.getGroupsDump = async () => {
      downloadUrl('/dump/group', 'contact-database-groups.xlsx')
    }

    this.getKeywordsDump = async () => {
      downloadUrl('/dump/keyword', 'contact-database-keywords.xlsx')
    }

    this.getDiseasesDump = async () => {
      const date = new Date()
      const dateStr = `${date.getFullYear()}-${fill(date.getMonth() + 1, 2, '0')}-${fill(date.getDate(), 2, '0')}`
      downloadUrl('/dump/disease', `diseases-${dateStr}.csv`)
    }

    this.getEventsDump = async () => {
      downloadUrl('/dump/event', 'contact-database-events.xlsx')
    }

    this.getOrganisationsDump = async () => {
      downloadUrl('/dump/organisation', 'contact-database-organisations.xlsx')
    }

    this.getWebsiteMembers = async () => {
      const membershipName = document.getElementsByName("membership")[0].value
      downloadUrl(`/report/${membershipName}-members.xlsx`, `${membershipName}-members.xlsx`)
    }

    this.getMembersStats = async () => {
      const membershipName = document.getElementsByName("membership")[0].value
      downloadUrl(`/report/${membershipName}-members-stats.xlsx`, `${membershipName}-members-stats.xlsx`)
    }

    this.getRepresentativesStats = async () => {
      const date = new Date()
      const reportPeriod = `${date.toLocaleString('default', {month: 'long'})}-${date.getFullYear()}`
      downloadUrl('/report/representatives-stats.xlsx', `representatives-stats-${reportPeriod}.xlsx`)
    }

    this.compareGroupings = async (e) => {
      const formData = new FormData()
      const file = this.refs['grouping-file'].files[0]
      formData.append('data', file)
      const response = apiResponse(await api(`/compare-groupings`, { body: formData }))
      if (response.ok) download(`grouping-compared.csv`, response.data, response.headers)
    }

    this.upload = async (e) => {
      const formData = new FormData()
      const file = this.refs.file.files[0]
      formData.append('data', file)
      const response = apiResponse(await api(`/person/import`, { body: formData, loader: true }))
      if (!response.ok) return
      const created = response.data.filter(item => item.status === 'created') || []
      const found = response.data.filter(item => item.status === 'found') || []
      const error = response.data.filter(item => item.status === 'error') || []
      const warnings = found.filter(item => Object.keys(item.report).includes('warning'))
      this.errorSummary = `${response.data.length} contacts were imported: ${created.length} created, ${found.length} existing and ${error.length} errors.`
      notify(this.errorSummary, error.length ? 'warning' : 'success')
      this.errors = error
      this.warnings = warnings
      downloadJsonReportToCsv(response.data, file.name, 'admin')
      this.update()
    }
});

riot.tag2('app', '<toast></toast> <header class="row spaced"> <h1 id="logo"> <a href="/"><img src="/src/images/logo.svg" alt="EURORDIS Contact Database"></a> <a if="{this.ghostMode}" ref="ghost" href="/admin" title="You\'re a Ninja! Go to the admin to disable."> <img src="/src/images/ninja.svg" alt="Ghost most, whoooouuu!"> </a> </h1> <div class="row secondary"> <div ref="loading"> <div> <p>Loading</p> <small>Results are on the way!</small> </div> <img src="/src/images/loader.svg" alt="Hello!"> </div> <search-box q="{this.q}" filters="{this.filters}"></search-box> <nav> <basket-link title="View export list"></basket-link> <hr> <a class="button plain" href="{getUrl(\'/person/new\')}"><icon figure="person-add-menu" title="Add a contact"></icon></a> <a class="button plain" href="{getUrl(\'/organisation/new\')}"><icon figure="organisation-add-menu" title="Add an organisation"></icon></a> <a class="button plain" href="{getUrl(\'/event/new\')}"><icon figure="event-add-menu" title="Add an event"></icon></a> <hr> <a class="button plain" href="?event_kind=summer_school%7CSummer%20School%7C%7Cdigital_school%7CDigital%20School%7C%7Cleadership_school%7CLeadership%20School%7C%7Cwinter_school%7CWinter%20School&event_role=Participant%7CParticipant%7C%7CObserver%7CObserver%7C%7CFellow%7CFellow%7C%7CResearcher%7CResearcher&showFacets=true"><icon figure="schoolhat-menu" title="Display all Alumni"></icon></a> <a class="button plain" href="?resource=person%7Cperson&group_role=Patient%20Representative%7CPatient%20Representative&group_kind=epag%7CERN%20-%20European%20Reference%20Network&showFacets=true"><icon figure="epag-menu" title="Display all ePag members"></icon></a> <a class="button plain" href="{this.getEurordisMembershipUrl()}"><icon figure="member-eurordis-menu" title="Display all EURORDIS members"></icon></a> <a class="button plain" href="https://gitlab.com/eurordis/contact-database/api/-/wikis/home" title="Help about this application" target="_blank"><icon figure="help-menu"></icon></a> </nav> </div> </header> <aside class="{resize-aside: this.page != \'home\' || this.filters.length}"> <results results="{this.results}" total="{this.total}" persons-total="{this.personsTotal}" organisations-total="{this.organisationsTotal}" query="{this.resultsQuery}" selected-pk="{this.instance && this.instance.pk}" filters="{this.filters}"></results> <section if="{!this.results || !this.results.length}"> <h1>Hello {this.user && this.user.props.first_name  || \'there\'}!</h1> <p>We are ready to go, just fill the search form!</p> <a href="/admin" title="Hey! Want to join the admin love? Give me 5 and dive in❣️ 🔖"><img src="/src/images/illustration-hello.svg" alt="Hello!"></a> </section> </aside> <home if="{this.page == \'home\'}"></home> <main if="{this.page != \'home\' || this.filters.length}"> <filters class="filters" if="{this.filters.length}" filters="{this.filters}" facets="{this.facets}" toggle="{this.toggleFilters}" facets-visible="{this.showFacets}"></filters> <div class="wrapper"> <login if="{this.page == \'login\'}"></login> <admin if="{this.page == \'admin\'}"></admin> <history if="{this.page == \'history\'}" changes="{this.changes}"></history> <activity if="{this.page == \'activity\'}" activities="{this.activities}"></activity> <facets-details show="{this.showFacets}" filters="{this.filters}" facets="{this.facets}" total="{this.total}"></facets-details> <div hide="{this.showFacets}"> <disease-details if="{this.page == \'disease\'}" disease="{this.instance}" diffs="{this.diffs}"></disease-details> <user-details if="{this.page == \'person\'}" user="{this.instance}" diffs="{this.diffs}"></user-details> <user-edit if="{this.page == \'person-edit\'}" user="{this.instance}"></user-edit> <organisation-details if="{this.page == \'organisation\'}" organisation="{this.instance}" diffs="{this.diffs}"></organisation-details> <organisation-edit if="{this.page == \'organisation-edit\'}" organisation="{this.instance}"></organisation-edit> <organisation-fees-edit if="{this.page == \'organisation-fees-edit\'}" organisation="{this.instance}"></organisation-fees-edit> <event-details if="{this.page == \'event\'}" event="{this.instance}" diffs="{this.diffs}"></event-details> <event-edit if="{this.page == \'event-edit\'}" event="{this.instance}"></event-edit> <error404 if="{this.page == \'error404\'}"></error404> <export-list if="{this.page == \'export-list\'}" resources="{this.exportResources}" limit="{this.limit}"></export-list> </div> </div> </main>', 'app,[data-is="app"]{ display: grid; grid-template-areas: \'header header\' \'aside main\'; grid-template-rows: auto calc(100vh - 5rem); grid-template-columns: auto 1fr; align-content: start; } app > *:not(toast),[data-is="app"] > *:not(toast){ z-index: 2; background-color: var(--secondary-background-color); } app main .wrapper,[data-is="app"] main .wrapper{ padding: 2rem 1.6rem 1rem; } app .filters,[data-is="app"] .filters{ position: sticky; padding: 1rem 1rem 0 1rem; border-bottom: 1px solid var(--border-color); top: 0; z-index: 20; background-color: #fff; width: 100%; } app home,[data-is="app"] home{ z-index: 1; } app > header,[data-is="app"] > header{ grid-area: header; margin: 0 !important; padding: 1rem 1.3rem; height: 5rem; background-color: var(--primary-background-color); border-bottom: 1px solid var(--border-color); z-index: 3; } app > header .secondary,[data-is="app"] > header .secondary{ align-items: flex-start; } app > header nav,[data-is="app"] > header nav{ display: flex; } app > header nav > *,[data-is="app"] > header nav > *{ margin: 0 .6rem 0 .6rem; } app > header nav hr,[data-is="app"] > header nav hr{ border-left: 1px solid var(--border-color); border-width: 0 0 0 1px; } app > header .button,[data-is="app"] > header .button{ padding: 0 !important; height: auto; border-radius: 0; } app > header .button > icon,[data-is="app"] > header .button > icon{ height: auto; margin-right: 0; } app > header .button > icon img,[data-is="app"] > header .button > icon img{ width: auto !important; } app > aside,[data-is="app"] > aside{ grid-area: aside; font-weight: 500; overflow-y: scroll; border-right: 1px solid var(--border-color); min-width: 35vw; max-width: 35vw; } app > .resize-aside,[data-is="app"] > .resize-aside{ resize: horizontal; min-width: 10vw; max-width: 80vw; width: 35vw; } app > aside > section,[data-is="app"] > aside > section{ padding-top: 4rem; text-align: center; color: var(--primary-color); } app > aside > section h1,[data-is="app"] > aside > section h1{ font-family: "IBMPlexSans", sans-serif; font-size: 2rem; font-weight: 500; } app > aside > section p,[data-is="app"] > aside > section p{ margin-bottom: 2.4rem; font-size: 0.9rem; font-weight: normal; } app > main,[data-is="app"] > main{ grid-area: main; overflow-y: scroll; } app #logo,[data-is="app"] #logo{ color: var(--primary-color); font-size: 1.3rem; font-weight: normal; position: relative; } app #logo img,[data-is="app"] #logo img{ height: 3rem; vertical-align: top; margin-right: .5rem; } app #logo a,[data-is="app"] #logo a{ color: inherit; text-decoration: none; } @keyframes bounce { 30% { transform: translateX(30px); } 75% { transform: translateX(20px) scaleX(-1); } 76% { transform: scaleX(1); } 33%, 60% { transform: translateX(-10px); } 75% { transform: translateY(0); } 80% { transform: translateY(10px); } 85% { transform: translateY(-50px); } 20% { transform: translateX(-20px); } 38%, 45% { transform: scaleX(1); } 40% { transform: scaleX(-1); } } app #logo [ref=ghost],[data-is="app"] #logo [ref=ghost]{ position: absolute; opacity: 0.95; top: 0px; left: -16px; height: 115px; animation-duration: 5s; animation-name: bounce; animation-iteration-count: infinite; } app #logo [ref=ghost] img,[data-is="app"] #logo [ref=ghost] img{ height: 100%; } app .help,[data-is="app"] .help{ position: absolute; bottom: 3rem; right: -1.4rem; display: block; padding: .3rem 1rem; color: inherit; text-decoration: none; font-size: .6rem; background-color: var(--primary-background-color); border: 1px solid #999; transform: rotate(-90deg); } app [ref=loading],[data-is="app"] [ref=loading]{ align-items: center; margin-right: 1.5rem !important; font-size: 0.6rem; color: var(--primary-color); display: none; } app [ref=loading] div,[data-is="app"] [ref=loading] div{ display: flex; flex-direction: column; align-items: flex-end; } app [ref=loading] p,[data-is="app"] [ref=loading] p{ margin: 0; line-height: 1; font-weight: bold; } app [ref=loading] small,[data-is="app"] [ref=loading] small{ font-size: 0.6rem; } app [ref=loading] img,[data-is="app"] [ref=loading] img{ width: 1.7rem; height: 1.7rem; margin-left: 0.4rem; } app.loading [ref=loading],[data-is="app"].loading [ref=loading]{ display: flex; }', '', function(opts) {
    window.app = this
    this.cachedFilters = ''

    riot.tag('raw', '', function(opts) {
      this.on('*', () => {
        this.root.innerHTML = opts.html
      })
    })

    this.on('mount', () => {
      this.setAsideSize()
      new ResizeObserver(this.updateAsideSize).observe(document.getElementsByTagName("aside")[0])
    })

    this.getEurordisMembershipUrl = () => {
      if(!config.EURORDIS_ORGANISATION) return
      return replaceQueryParams({ membership: `${config.EURORDIS_ORGANISATION.pk}|EURORDIS`, resource: 'organisation', showFacets: true })
    }

    this.showFacets = this.showFacets || false
    this.filters = []

    this.setPage = (name) => {
      hideLoader()
      this.showFacets = false
      this.showFacets = 'showFacets' in getQueryParams()
      this.page = name
      this.update()
    }

    this.setAsideSize = () => {
      let asideSize = localStorage.getItem('asideSize')
      let asideElement = document.getElementsByClassName('resize-aside')[0]
      if (asideSize && asideElement) {
        asideElement.style.width = `${asideSize}px`
      }
    }

    this.updateAsideSize = (event) => {
      if (this.page != 'home' || this.filters.length) {
        let asideSize = event[0].target.offsetWidth
        localStorage.setItem('asideSize', asideSize)
      }
    }

    page('*', async (req, next) => {
      this.ghostMode = !!localStorage.getItem('ghostMode')
      this.user = user
      showLoader()
      this.q = req.query.get('q')
      const limit = req.query.get('limit')

      const filterNames = ['resource', 'disease', 'grouping', 'keyword', 'group', 'membership',
                           'group_role', 'country', 'contact', 'kind', 'event', 'event_kind', 'event_role', 'keyword_kind', 'group_kind']
      const filters = []
      filterNames.forEach(name => {
        const rawValues = req.query.get(name)
        if (!rawValues) return
        rawValues.split('||').forEach(rawValue => {
          let [value, label] = rawValue.split('|')

          const maybeModifier = value.slice(0, 2).replace(' ', '+')
          value = maybeModifier + value.slice(2)
          filters.push({ name, value, label })
        })
      })

      if (limit === 'false') filters.push({ name: 'limit', value: 99999 })

      this.filters = filters.map(f => f)

      if(user.isAuthenticated() && (JSON.stringify({ filters: this.filters }) !== this.cachedFilters))
        await this.updateFilters(filters)
      this.cachedFilters = JSON.stringify({ filters: this.filters })

      this.diffs = null

      next()
    })

    page('*', function activityStats(req, next) {
      const response = api('/activity', { data: {
        by: { label: user.props.label, pk: user.props.pk },
        location: page.current,
      }})
      next()
    })

    this.updateFilters = async (filters = []) => {
      let response = { data: [], facets: { resource: [] }, total: 0 }
      const hasQueryFilters = (
        filters.length &&
        !(filters.length === 1 && ['limit', 'resource'].includes(filters[0].name))
      )
      if(hasQueryFilters) response = await searchResults(filters)
      if(!response) return
      this.results = response.data
      this.facets = response.facets
      this.total = response.total
      const personsResource = this.facets.resource.find(r => r.pk === 'person')
      this.personsTotal = personsResource && personsResource.count
      const organisationsResource = this.facets.resource.find(r => r.pk === 'organisation')
      this.organisationsTotal = organisationsResource && organisationsResource.count
    }

    function authRequired(req, next) {
      if(!user.isAuthenticated()) {
        notify('You must login to access this page', 'warning')
        return page('/login')
      }
      next()
    }

    page('/login', () => {
      if(user.isAuthenticated()) return page('/')
      this.setPage('login', true)
    })

    page('/access/:token', async (req) => {
      localStorage.setItem('token', req.params.token)
      await user.authenticate(req.params.token)
      if(!user.isAuthenticated()) return notify('Your token is not valid', 'error')
      notify(`Welcome ${ user.get('label') }, you are now authenticated!`, 'success')
      page('/')
    })

    page('/', authRequired, () => this.setPage('home', true))

    page('/admin', authRequired, () => this.setPage('admin'))

    page('/logout', authRequired, () => {
      localStorage.removeItem('token')
      user.logout()
      notify('You are logged out', 'success')
      page('/login')
    })

    page('/organisation/:pk/edit/fees', authRequired, async (req) => {
      const response = apiResponse(await api(`/organisation/${req.params.pk}`))
      this.instance = response.data
      this.setPage(`organisation-fees-edit`)
    })

    page('/:type/:pk/edit', authRequired, async (req) => {
      const type = req.params.type
      const response = apiResponse(await api(`/${type}/${req.params.pk}`))
      this.instance = response.data
      this.setPage(`${type}-edit`)
    })

    page('/:type/new', authRequired, async (req) => {
      const type = req.params.type
      const defaults = {
        person: { languages: [], keywords: [], diseases: [], roles: [] },
        organisation: { diseases: [], keywords: [], groupings: [], roles: [], membership: [] },
        event: {}
      }
      this.instance = defaults[type]
      this.setPage(`${type}-edit`)
    })

    page('/:type/:pk/:history?', authRequired, async (req) => {
      const type = req.params.type
      const response = apiResponse(await api(`/${type}/${req.params.pk}`))
      this.instance = response.data

      if(req.params.history === 'history') {
        const diffsResponse = apiResponse(await api(`/${req.params.type}/${req.params.pk}/history`))
        if(!response.ok) return
        this.diffs = diffsResponse.data.data
      }
      this.setPage(type)
    })

    page('/export-list', authRequired, async (req) => {
      const roles = exportList.all().map(r => ({ person: r.person, organisation: r.organisation }))
      const response = apiResponse(await api('/basket', { data: {resources: roles }}))
      this.exportResources = response.data.data
      const limitFilter = this.filters.find(f => f.name === 'limit')
      this.limit = limitFilter && limitFilter.value
      this.setPage('export-list')
    })

    page('/history', authRequired, async (req) => {
      const params = page.current.split('?')[1]
      const response = apiResponse(await api(`/history?limit=1000&${params}`))
      this.changes = response.data.data
      this.setPage('history')
    })

    page('/activity', authRequired, async (req) => {
      const response = apiResponse(await api(`/activity?limit=1000`))
      this.activities = response.data.data
      this.setPage('activity')
    })

    page('*', () => this.setPage('error404'))

    init(page)

    this.toggleFilters = () => {
      this.showFacets = !this.showFacets
      page(replaceQueryParams({ showFacets: this.showFacets }))
      this.update()
    }
});

riot.tag2('attachment-link', '<a href="{this.url}" class="{plain: true, deleted: this.deleted}" onclick="{this.download}" title="Download this file"> {this.filename} </a> <button if="{this.opts.deletable && !this.deleted}" class="delete" onclick="{this.delete}" url="/contact/{this.organisation.pk}/attachment/{attachment.id}" title="Delete this file"><icon figure="delete"></icon>Delete</button>', 'attachment-link { display: flex; align-items: center; } attachment-link button,[data-is="attachment-link"] button{ margin-left: 1rem; } attachment-link .deleted,[data-is="attachment-link"] .deleted{ text-decoration: line-through; opacity: .8; }', '', function(opts) {
    this.url = ""
    this.deleted = false
    this.attachment = {}
    this.filename = ""

    this.on('*', () => {
      this.attachment = this.opts.attachment
      this.filename = this.attachment.filename
      this.url = `/contact/${ this.opts.contactId }/attachments/${ this.attachment.id }`
    })

    this.download = async (event) => {
      event.preventDefault()
      const response = await api(this.url)
      download(response.headers.get('X-Filename'), response.data, response.headers)
    }

    this.delete = async (event) => {
      event.preventDefault()
      if(!confirm('Are you sure you want to permanently delete this file?')) return
      const response = apiResponse(await api(this.url, { method: 'delete' }))
      if(response.ok) {
        this.deleted = true
        notify(`Document "${ this.filename }" was deleted.`, 'success')
      }
      this.update()
    }
});

riot.tag2('basket-link', '<a href="{getUrl(\'/export-list\')}"> <icon figure="basket-menu"></icon><span if="{this.count}">{this.count}</span> </a>', 'basket-link a,[data-is="basket-link"] a{ position: relative; } basket-link span,[data-is="basket-link"] span{ position: absolute; top: -1rem; right: 0; display: inline-block; min-width: .8rem; padding: .05rem .15rem; color: white; font-size: .45rem; text-align: center !important; font-weight: bold; border-radius: .4rem; background-color: var(--secondary-color); }', '', function(opts) {
    this.on('*', () => {
      this.count = window.exportList && window.exportList.all().length
    })
});

riot.tag2('bulk-log', '<div if="{this.errors.length}" class="import-error"> <h3 class="error"> <icon figure="message-alert-small"></icon> Report: {this.opts.title} </h3> <ul> <li each="{error in this.errors}"> <a if="{error.data.uri}" href="{error.data.uri}" target="_blank">{error.raw.first_name} {error.raw.last_name} - {error.raw.email           || \'Empty email\'}</a> <strong if="{!error.data.uri}">{error.raw.first_name} {error.raw.last_name} - {error.raw.email           || \'Empty email\'}</strong>: <ul> <li each="{msg, key in error.report}">{key}: {msg}</li> <li each="{msg, key in error.errors}"><span if="{key}">{key}: </span>{msg}</li> </ul> </li> </ul> </div>', '', '', function(opts) {
    this.errors = []

    this.on('*', () => {
      this.errors = this.opts.errors || []
    })
});

riot.tag2('bulk-warning', '<div if="{opts.warnings && opts.warnings.length}" class="import-error"> <h3 class="error"> <icon figure="message-alert-small"></icon> Warning: {this.opts.title || ⁗The following contacts were partially saved⁗} </h3> <table> <tr> <th>Contact</th> <th>Warning</th> </tr> <tr each="{warning in opts.warnings}"> <td><a href="{warning.data.uri + location.search}" target="_blank">{warning.raw.first_name} {warning.raw.last_name}<br> <small>{warning.raw.email || \'Empty email\'}</small></a> </td> <td> <div each="{msg, key in warning.report}">{msg}</div> </td> </tr> </table> </div>', 'bulk-warning tr,[data-is="bulk-warning"] tr{ text-align: left; vertical-align: top; } bulk-warning tr:not(:last-child) td,[data-is="bulk-warning"] tr:not(:last-child) td{ padding: .3rem .1rem; border-bottom: 1px solid #eee; }', '', function(opts) {
});

riot.tag2('contact-attachment-edit', '<fieldset> <div> <div> <details> <summary> <legend>Documents <small if="{this.contact.attachments}">({this.contact.attachments.length})</small></legend> Expand </summary> <div each="{attachment in this.contact.attachments}" class="attachment row"> <attachment-link contact-id="{this.contact.pk}" attachment="{attachment}" deletable="{true}"></attachment-link> </div> <div> <label for="comment">Add a document</label> <input type="file" name="attachment" placeholder="Attach a document" onchange="{this.prepare}"> </div> </details> </div> </div> </fieldset>', '', '', function(opts) {
    this.contact = {}
    this.attachment = null

    this.on('*', () => {
      this.contact = this.opts.contact
    })

    this.prepare = (event) => {
      this.attachment = event.target.files[0]
    }

    this.save = async (contact) => {
      if(!contact) contact = this.contact
      if (!this.attachment) return
      const fdata = new FormData()
      fdata.append('data', this.attachment)
      const resp = await api(`/contact/${contact.pk}/attachments`, { body: fdata })
    }
});

riot.tag2('country-flag', '<img if="{this.country}" alt="{this.country}" width="16" title="{this.country}" riot-src="/src/images/flags/{this.country}.svg">', 'country-flag,[data-is="country-flag"]{ display: flex; align-items: center } country-flag img,[data-is="country-flag"] img{ display: inline-block; vertical-align: middle; max-height: 20px; }', '', function(opts) {
    this.on('*', () => {
      const country = this.opts.country
      this.country = country && country.toLowerCase()
    })
});

riot.tag2('diff', '<p class="{opts.isAdd ? \'change-add\' : \'change-remove\'}"> <span if="{opts.diffObject.constructor === Object}" each="{changeField in Object.keys(opts.diffObject)}"><b><change-field field="{changeField}"></change-field></b>: {JSON.stringify(opts.diffObject[changeField])}</span> <span if="{opts.diffObject.constructor !== Object}">{opts.diffObject}</span> </p>', 'diff .change-add span:not(:last-child):after,[data-is="diff"] .change-add span:not(:last-child):after,diff .change-remove span:not(:last-child):after,[data-is="diff"] .change-remove span:not(:last-child):after{ content: " ; "; } diff .change-add span,[data-is="diff"] .change-add span{ background-color: #beffbe; } diff .change-remove span,[data-is="diff"] .change-remove span{ background-color: #ffbebe; } diff .change-add b,[data-is="diff"] .change-add b,diff .change-remove b,[data-is="diff"] .change-remove b{ font-weight: 500; }', '', function(opts) {
    riot.tag('change-field', '', function(opts) {
      this.on('*', () => {
        this.root.innerHTML = opts.field.replace(/_./, function(str){ return ` ${str[1].toUpperCase()}` }).replace(/^./, function(str){ return str.toUpperCase() })
      })
    })
});
riot.tag2('diffs', '<h2>Previous changes</h2> <ul> <li each="{diff in this.diffs}"> <details open="{!this.collapse}"> <summary> <h3> <a target="_blank" href="{getUrl(\'/person/\'+diff.by.pk)}">{diff.by.label}</a> {diff.action}d&nbsp;<a if="{this.opts.withcontext}" target="_blank" href="{diff.of.uri}">{diff.of.label}</a> on {trans(new Date(diff.at), { time: true })} </h3> </summary> <div each="{change in diff.changes}"> <em>{change.verb}d {change.subject}</em> : <div each="{diff in change.diffs}"> <diff if="{diff.newDiff && !Array.isArray(diff.newDiff)}" is-add="{true}" diff-object="{diff.newDiff}"></diff> <diff if="{diff.newDiff && Array.isArray(diff.newDiff)}" each="{newDiff in diff.newDiff}" is-add="{true}" diff-object="{newDiff}"></diff> <diff if="{diff.oldDiff && !Array.isArray(diff.oldDiff)}" is-add="{false}" diff-object="{diff.oldDiff}"></diff> <diff if="{diff.oldDiff && Array.isArray(diff.oldDiff)}" each="{oldDiff in diff.oldDiff}" is-add="{false}" diff-object="{oldDiff}"></diff> </div> </div> </details> </li> </ul> <p if="{!this.diffs.length}">This record has not been recently changed.</p>', 'diffs li,[data-is="diffs"] li{ display: block; } diffs summary:focus,[data-is="diffs"] summary:focus{ outline: none; } diffs h3,[data-is="diffs"] h3{ padding-bottom: .5rem; padding-top: 1rem; font-size: 0.8rem; font-family: "IBMPlexSans", sans-serif; font-weight: 300; } diffs summary,[data-is="diffs"] summary{ display: flex; align-items: center; font-weight: 500; } diffs summary:before,[data-is="diffs"] summary:before{ order: 2; margin-left: 0.8rem; } diffs p,[data-is="diffs"] p{ margin: 0; } diffs details > div,[data-is="diffs"] details > div{ margin-top: 10px; }', '', function(opts) {
    this.diffs = []

    function fixValue(val) {
      const dVal = new Date(val)
      if(dVal != 'Invalid Date' && dVal.getFullYear() > 2000) return new Date(val)
      return val
    }

    function appendChange(changes, diff, subject = '') {
      let diffs = []
      const verb = diff.verb
      let newDiff = null
      if (verb == 'create' || verb == 'update') newDiff = diff.new || diff.value
      let oldDiff = diff.old

      if (Array.isArray(newDiff) && Array.isArray(oldDiff)) {
        oldDiff = diffing(newDiff, diff.old)
        newDiff = diffing(diff.old, newDiff)
        let allKeys = new Set([...Object.keys(oldDiff), ...Object.keys(newDiff)])
        allKeys.forEach(key => {
          diffs.push({newDiff: newDiff[key], oldDiff: oldDiff[key]})
        })
      } else {
        diffs.push({newDiff, oldDiff})
      }

      if(diff.length) diff.forEach(d => appendChange(changes, d, `${subject} ${d.subject}`))
      else if (diff.subordinates) diff.subordinates.forEach(sub => appendChange(changes, sub, `${subject} ${sub.subject}`))
      else changes.push({ subject, verb, diffs })
    }

    this.on('*', () => {
      this.collapse = this.opts.collapse
      this.diffs = []
      this.opts.diffs.forEach(container => {
        const changes = []
        Object.keys(container.diff).map(prop => {
          let change = container.diff[prop]
          let subject = prop.slice(-1) == 's' ? prop.slice(0, -1) : prop
          appendChange(changes, change, `[${subject}]`)
        })
        this.diffs.push({ by: container.by, of: container.of, at: container.at, action: container.action, changes })
      })
    })

});

riot.tag2('disease-details', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-disease"></icon> </div> </div> <div> <h1><disease-gauge depth="{this.opts.disease.depth}" max-depth="{this.opts.disease.max_depth}"></disease-gauge> {this.opts.disease.label}</h1> <p> Orphanet: <a href="https://www.orpha.net/consor/cgi-bin/OC_Exp.php?Expert={this.opts.disease.pk}" title="View on Orphanet" target="_blank">{this.opts.disease.pk}</a></span> </p> </div> </header> <section> <ul> <li class="grouping" if="{this.opts.disease.groupings.length}"> <grouping-tag each="{grouping in this.opts.disease.groupings}" riot-value="{grouping.name}"></grouping-tag> </li> <li>Classification level:&nbsp;<span> {this.groupOfType} ({this.opts.disease.disorder_type}) </span></li> <li><icon figure="gauge-1"></icon> Classification: <ul> <li each="{root_ in this.opts.disease.roots}"> <tag type="disease" label="{root_.name}" external-url="https://www.orpha.net/consor/cgi-bin/OC_Exp.php?Expert={root_.pk}" external-title="View details on Orphanet" riot-value="{root_.pk}" details-link="/disease/{root_.pk}"></tag> </div> </li> </ul> </li> <li if="{this.opts.disease.synonyms}"><icon figure="synonyms"></icon>Synonyms:&nbsp;<span> {this.opts.disease.synonyms.join(\', \')} </span></li> </ul> </section> <section> <fieldset> <h3>Manage Groupings</h3> <form onsubmit="{this.saveGroupings}"> <table> <thead> <tr> <th>Assigned</th> <th>Validated</th> <th>Name</th> </tr> </thead> <tbody> <tr each="{grouping in this.configGroupings}"> <td> <input type="checkbox" name="grouping" checked="{this.hasGrouping(grouping.key)}" riot-value="{grouping.key}" id="grouping_{grouping.key}" onchange="{this.addGrouping}"> <label for="grouping_{grouping.key}"></label> </td> <td> <input type="checkbox" name="grouping_validate" riot-value="{grouping.key}" checked="{this.isValidated(grouping.key)}" id="grouping_validate_{grouping.key}" disabled="{!this.hasGrouping(grouping.key)}"> <label for="grouping_validate_{grouping.key}"></label> </td> <td> {grouping.value} <small>– {grouping.key}</small> </td> </tr> </span> </tbody> </table> <button class="primary row">Save</button> </form> </fieldset> </section> <diffs if="{this.opts.diffs}" diffs="{this.opts.diffs}"></diffs> <section class="toolbar"> <small> <a href="{\'/disease/\' + this.opts.disease.pk + \'/history\'}">Blame who messed with this</a> </small> </section> <link rel="stylesheet" href="/src/css/details.css">', 'disease-details h1,[data-is="disease-details"] h1,disease-details header a,[data-is="disease-details"] header a,disease-details header a:hover,[data-is="disease-details"] header a:hover{ color: var(--disease); } disease-details .row,[data-is="disease-details"] .row{ margin-top: 0; margin-bottom: 1.6rem; } disease-details h1 + p,[data-is="disease-details"] h1 + p{ margin: 0; } disease-details header a,[data-is="disease-details"] header a{ text-decoration: underline; } disease-details header a:hover,[data-is="disease-details"] header a:hover{ background: none; } disease-details section,[data-is="disease-details"] section{ font-weight: 500; } disease-details section:not(.toolbar):before,[data-is="disease-details"] section:not(.toolbar):before{ background-color: transparent !important; } disease-details ul ul,[data-is="disease-details"] ul ul{ flex-basis: 100%; margin-top: 0.4rem !important; margin-left: 1.4rem !important; } disease-details li,[data-is="disease-details"] li{ flex-wrap: wrap; font-weight: 500; } disease-details li span,[data-is="disease-details"] li span{ font-weight: normal; } disease-details form button,[data-is="disease-details"] form button{ margin-top: 1rem !important; } disease-details th,[data-is="disease-details"] th{ padding-right: 3rem; text-align: left; } disease-details td label::before,[data-is="disease-details"] td label::before{ top: -8px !important; } disease-details td label::after,[data-is="disease-details"] td label::after{ top: -7px !important; }', 'class="details"', function(opts) {
    this.on('mount', () => {
      this.groupOfType = config.DISEASE_GROUP_OF_TYPES[this.opts.disease.group_of_type]
      this.configGroupings = config.GROUPING.sort((a, b) => a.value < b.value ? -1 : 1)
      this.update()
    })

    this.hasGrouping = (name) => {
      return !!this.opts.disease.groupings.find(g => g.name === name)
    }

    this.isValidated = (name) => {
      return !!this.opts.disease.groupings.find(g => g.name === name)?.validated
    }

    this.addGrouping = (event) => {
      const groupingValidate = this.root.querySelector(`#grouping_validate_${event.target.value}`)

      if (event.target.checked)
        groupingValidate.disabled = false;
      else {
        groupingValidate.checked = false;
        groupingValidate.disabled = true;
        this.opts.disease.groupings = this.opts.disease.groupings.filter(g => g.name !== event.target.value);
      }
    }

    this.saveGroupings = async (event) => {
      event.preventDefault()
      const data = new FormData(event.target)
      const groupingValidates = data.getAll('grouping_validate')
      const updateGroupings = data.getAll('grouping').reduce((acc, name) => {
        return [...acc, {name, validated: !!groupingValidates.includes(name)}]
      }, [])
      const response = apiResponse(await api(`/disease/${this.opts.disease.pk}`, { method: 'patch', data: { groupings: updateGroupings } }))
      if (response.ok) {
        page(`/disease/${this.opts.disease.pk}`)
        notify(`Groupings were saved successfully`, 'success')
      }
    }
});

riot.tag2('disease-gauge', '<icon if="{this.level}" figure="gauge-{this.level}"></icon>', '', '', function(opts) {
    this.level = null

    this.on('mount', () => {
      const depth = this.opts.depth
      const maxDepth = this.opts.maxDepth
      if (!(depth !== undefined && maxDepth !== undefined)) return

      if (depth === 0) this.level = 1
      else if (depth === maxDepth) this.level = 5

      else this.level = 2 + Math.round(depth * 2 / maxDepth)
      this.update()
    })
});

riot.tag2('duplicates', '<blockquote if="{this.duplicates.length}" class="{this.type}"> <p>{this.label}</p> <ul> <li each="{record in this.duplicates}"><a href="{record.uri}" target="_blank">{record.label}</a></li> </ul> </blockquote>', 'duplicates blockquote,[data-is="duplicates"] blockquote{ background-color: #c7e4ff; padding: .5rem 1.5rem; margin: 1rem 0; } duplicates blockquote.error,[data-is="duplicates"] blockquote.error{ background-color: #D20019; color: #fff; }', '', function(opts) {
    this.duplicates = []
    this.href = ''
    this.url = ''
    this.defaultType = ''
    this.defaultLabel = 'The records below seem very similar. Are you sure this is not a duplicate?'

    this.on('update', async () => {
      if(this.opts.href === this.href) return
      this.href = this.opts.href
      this.duplicates = []
      this.label = ''
      if(!this.href) return this.update()

      const response = apiResponse(await api(this.href))
      this.duplicates = response.data.data ? response.data.data.filter(d => d.score >= .67) : []
      if(this.opts.currentId) this.duplicates = this.duplicates.filter(d => d.pk != this.opts.currentId)
      if(this.opts.resource) this.duplicates = this.duplicates.filter(d => d.resource === this.opts.resource)
      if(this.opts.exact == 'error' && response.data.data.length && response.data.data[0].score >= .98) this.type = 'error'
      else this.type = this.defaultType
      this.type = this.opts.exact == 'error' && response.data.data.length && response.data.data[0].score >= .98
                ? 'error' : this.defaultType
      this.label = this.opts.label || this.defaultLabel
      this.update()
    })
});

riot.tag2('error404', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-error"></icon> </div> </div> <div> <h1>The page does not exist</h1> </div> </header> <section> <p> The page you are trying to reach does not exist.<br> <icon figure="arrow-right"></icon> <a href="/"> Back to home </a> </p> </section> <link rel="stylesheet" href="/src/css/details.css">', 'error404.details section:before,[data-is="error404"].details section:before{ background-color: transparent !important; } error404 a,[data-is="error404"] a{ color: var(--primary-color); border-bottom: none; vertical-align: bottom; }', 'class="details"', function(opts) {
});

riot.tag2('event-details', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-event"></icon> </div> </div> <div> <h1>{this.event.name}</h1> <h2 if="{this.event.kind}"> <icon figure="event_kind"></icon> {this.event.kind} </h2> </div> </header> <section> <div> <ul> <li if="{this.event.start_date}"> <icon figure="event"></icon> <div> Begins on {trans(new Date(this.event.start_date))} <span if="{this.event.duration}"> For {this.event.duration} days </span> </div> </li> <li if="{this.event.location}"> <icon figure="address"></icon> In {this.event.location} </li> </ul> <p> <icon figure="person"></icon> <a href="/?event={event.pk}|{event.label}">View attendees</a> <button id="download" onclick="{this.downloadAttendees}" title="Export all attendees of this event in a spreadsheet">Export as .xlsx</button> </p> </div> </section> <diffs if="{this.opts.diffs}" diffs="{this.opts.diffs}"></diffs> <section class="toolbar"> <small> id: {this.event.pk} <span if="{this.event.filemaker_pk}">(previously {this.event.filemaker_pk})</span> <p> Created on {trans(new Date(this.event.created_at))} <virtual if="{this.event.created_by}"> by <a href="/person/{this.event.created_by.pk}"> {this.event.created_by.first_name} {this.event.created_by.last_name} </a> </virtual> </p> <p> Modified on {trans(new Date(this.event.modified_at))} <virtual if="{this.event.modified_by}"> by <a href="/person/{this.event.modified_by.pk}"> {this.event.modified_by.first_name} {this.event.modified_by.last_name} </a> </virtual> </p> <a href="{getUrl(\'/event/\' + this.opts.event.pk + \'/history\')}">Blame who messed with this</a> </small> <a href="/event/{this.opts.event.pk}/edit/{location.search}" class="button"><icon figure="edit" embedded></icon> Edit this event</a> </section> <link rel="stylesheet" href="/src/css/details.css">', 'event-details.details section:not(.toolbar):before,[data-is="event-details"].details section:not(.toolbar):before{ background-color: transparent; } event-details :not(.toolbar) a,[data-is="event-details"] :not(.toolbar) a{ color: var(--primary-color) !important; } event-details li,[data-is="event-details"] li{ flex-wrap: wrap; } event-details li span,[data-is="event-details"] li span{ display: block; } event-details a,[data-is="event-details"] a{ border-bottom: none; font-weight: 500; } event-details #download,[data-is="event-details"] #download{ display: inline-block; margin-left: 1rem; }', 'class="details"', function(opts) {
    this.on('*', () => {
      this.event = this.opts.event
    })

    this.downloadAttendees = async () => {
      downloadUrl(`/event/${this.event.pk}/export`, `${ this.event.label }-attendees.xlsx`)
    }
});

riot.tag2('event-edit', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-event"></icon> </div> </div> <div> <h1 if="{event.pk}">Edit: {event.name}</h1> <h1 if="{!event.pk}">Create an Event</h1> </div> </header> <form onsubmit="{this.submit}" class="resource-edit-form"> <fieldset> <div> <div> <legend>Information</legend> <div class="row"> <div class="w70p"> <label for="name">Name of the event</label> <input class="row" name="name" riot-value="{event.name}" placeholder="Name" required onblur="{this.checkDuplicatesName}"> </div> <div class="fx-grow-2"> <label for>Kind of event</label> <select class="row" name="kind" required> <option value=""> --- </option> <option each="{kind in this.EVENT_KINDS}" riot-value="{kind.key}" selected="{this.event.kind == kind.key}">{kind.value}</option> </select> </div> </div> <div class="row w70p"> <div class="w50p"> <label for="start_date">Date of beginning</label> <input name="start_date" riot-value="{event.start_date}" placeholder="Date of beginning" required type="{\'date\'}"> </div> <div class="w50p"> <label for="duration">Duration of the event (in days)</label> <input name="duration" riot-value="{event.duration}" placeholder="Duration of the event (in days)" required type="{\'number\'}"> </div> </div> <label for="location">Location</label> <input class="row" name="location" riot-value="{event.location}" placeholder="Location" required> </div> </div> <duplicates if="{!this.event.pk && this.duplicatesNameUri}" href="{this.duplicatesNameUri}" resource="event"></duplicates> </fieldset> <fieldset if="{user.can(\'import\')}"> <div> <div> <legend>Attendees</legend> <p if="{!this.event.pk}">Import events in the next step</p> <div if="{this.event.pk}"> <div class="row align-end"> <div class="w100p"> <label>Import list of attendees</label> <input ref="file" type="file"> </div> <button class="button" type="button" onclick="{this.upload}"> <icon figure="add" embedded></icon> Import </button> </div> <bulk-log errors="{this.errors}" title="{this.errorSummary}"></bulk-log> <bulk-warning warnings="{this.warnings}" title="The event was linked to the following contacts, however…"></bulk-warning> </div> </div> </div> </fieldset> <section class="toolbar"> <div class="secondary"> <button if="{this.event.pk && this.user.can(\'delete\')}" type="button" class="plain" onclick="{this.delete}"><icon figure="delete-small"></icon>Delete</button> <button class="button" type="button" onclick="{() => history.back()}"><icon figure="close-small" embedded></icon>Cancel</button> <button class="button"><icon figure="save-small" embedded></icon>{this.event.pk ? \'Save\' : \'Next >\'}</button> </div> </section> </form> <link rel="stylesheet" href="/src/css/details.css">', 'event-edit [type="file"] + button,[data-is="event-edit"] [type="file"] + button{ margin-top: 0.8rem; } event-edit .import-error,[data-is="event-edit"] .import-error{ margin-top: 0.4rem; } event-edit .import-error,[data-is="event-edit"] .import-error,event-edit .import-error h3,[data-is="event-edit"] .import-error h3{ font-size: 0.6rem; } event-edit .import-error h3,[data-is="event-edit"] .import-error h3{ margin-bottom: 0.8rem; color:#D20019; font-weight: 500; } event-edit .import-error h3 icon,[data-is="event-edit"] .import-error h3 icon{ margin-right: 0.4rem; vertical-align: middle; } event-edit .import-error > ul > li,[data-is="event-edit"] .import-error > ul > li{ display: block; margin-bottom: 0.8rem; } event-edit .import-error > ul,[data-is="event-edit"] .import-error > ul{ margin-left: 0.4rem; padding-left: 0.8rem; border-left: 1px solid var(--border-color); } event-edit .import-error > ul > li li,[data-is="event-edit"] .import-error > ul > li li{ display: block; } event-edit .import-error a,[data-is="event-edit"] .import-error a{ font-weight: bold; }', 'class="details"', function(opts) {
    this.user = window.user
    this.event = {}
    this.EVENT_KINDS = config.EVENT_KINDS

    this.on('*', () => {
      this.event = this.opts.event
    })

    this.submit = async (e) => {
      e.preventDefault()
      const form = e.target
      const data = serializeForm(form)
      const response = this.event.pk
                     ? apiResponse(await api(`/event/${this.event.pk}`, { data, method: 'put' }))
                     : apiResponse(await api('/event', { data }))
      if (response.ok) {
        const event = response.data
        this.event.pk ? page(`/event/${event.pk}${location.search}`) : page(`/event/${event.pk}/edit${location.search}`)
        this.event = event
        notify('The event was saved', 'success')
        this.update()
      }
    }

    this.checkDuplicatesName = (event) => {
      if(this.event.pk) return
      this.duplicatesNameUri = `/complete?q=${event.target.value}&limit=3`
    }

    this.upload = async (e) => {
      const formData = new FormData()
      const file = this.refs.file.files[0]
      formData.append('data', file)
      const response = apiResponse(await api(`/event/${ this.event.pk }/import`, { body: formData, loader: true }))
      if(!response.ok) return
      const created = response.data.filter(item => item.status === 'created') || []
      const found = response.data.filter(item => item.status === 'found') || []
      const warnings = found.filter(item => Object.keys(item.report).includes('warning'))
      const error = response.data.filter(item => item.status === 'error') || []
      this.errorSummary = `${response.data.length} attendees were imported: ${created.length} created, ${found.length} existing and ${error.length} errors.`
      notify(this.errorSummary, error.length ? 'warning' : 'success')
      this.errors = error
      this.warnings = warnings
      downloadJsonReportToCsv(response.data, file.name, `event${this.event.pk}`)

      this.update()
    }

    this.delete = async (event) => {
      if(!confirm('You are about to permanently delete this event. Are your sure?')) return
      const response = apiResponse(await api(`/event/${this.event.pk}`, { method: 'delete' }))
      if (!response.ok) return
      notify(`The event "${this.event.name}" was deleted.`, 'success')
      page(`/${location.search}`)
    }
});

riot.tag2('expandable-list', '<yield></yield> <button if="{this.slice < this.opts.items.length}" onclick="{()=> this.slice = this.opts.items.length}"> See more </button> <button if="{this.slice == this.opts.items.length}" onclick="{()=> this.slice = this.initialSlice}"> See less </button>', '', '', function(opts) {
        this.initialSlice = this.opts.slice ? this.opts.slice : 6
        this.slice = this.initialSlice
});
riot.tag2('script', '', '', '', function(opts) {


    this.all = function() {
      return JSON.parse(localStorage.getItem('exportList')) || [];
    }.bind(this)

    this.cleanResource = function(resource) {
      const cleaned = {}
      if(resource.person && resource.person.pk) cleaned.person = resource.person.pk
      else if(resource.person) cleaned.person = resource.person

      if(resource.organisation && resource.organisation.pk) cleaned.organisation = resource.organisation.pk
      else if(resource.organisation) cleaned.organisation = resource.organisation

      return cleaned
    }.bind(this)

    this.getId = function(resource) {
      return `${resource.person}${resource.organisation}`
    }.bind(this)

    this.add = function(resource) {
      resource = this.cleanResource(resource)
      if (!Object.keys(resource).length || this.has(resource)) return
      const list = this.all()
      list.push(resource)
      notify(`Added 1 contact for export`, 'info')
      this.save(list)
    }.bind(this)

    this.addMany = function(resources) {
      resources = resources.map(this.cleanResource)
      const list = this.all()
      resources = resources.filter(resource => !list.map(l => this.getId(l)).includes(this.getId(resource)))
      const merged = list.concat(resources)
      notify(`Added ${resources.length} contact${resources.length == 1 ? '' : 's'} for export`, 'info')
      this.save(merged)
    }.bind(this)

    this.has = function(resource) {
      resource = this.cleanResource(resource)
      return this.findIndex(resource) > -1
    }.bind(this)

    this.findIndex = function(resource) {
      return this.all().findIndex(r => {
        if (r.person !== resource.person) return false
        return ((!r.organisation && !resource.organisation) || (r.organisation == resource.organisation))
      })
    }.bind(this)

    this.removeOrganisation = function(pk) {
      const all = exportList.all()
      const list = all.filter(r => r.organisation !== pk) || []
      const count = all.length - list.length
      if(count) this.save(list)
      notify(`removed ${count} contact${count == 1 ? '' : 's'} from export`, 'info')
    }.bind(this)

    this.remove = function(resource) {
      resource = this.cleanResource(resource)
      if (!this.has(resource)) return
      const list = this.all()
      list.splice(this.findIndex(resource), 1)
      notify(`removed 1 contact of export`, 'info')
      this.save(list)
    }.bind(this)

    this.removeIndex = function(index) {
      const list = this.all()
      list.splice(index, 1)
      notify(`removed 1 contact from export`, 'info')
      this.save(list)
    }.bind(this)

    this.save = function(list) {
      localStorage.setItem('exportList', JSON.stringify(list))
      updatePage()
    }.bind(this)

    this.clear = function() {
      this.save([])
      notify(`All contacts were cleared out of export`, 'info')
    }.bind(this)

    window.exportList = this
});

riot.tag2('export-list', '<header> <div class="row"> <h1>Exporting contacts</h1> <button onclick="{this.clear}" class="button" if="{this.resources && this.resources.length}"> <icon figure="delete" embedded></icon> Clear all contacts </button> </div> <div class="row" if="{this.resources && this.resources.length}"> <div class="results-number"> <span if="{this.resources.length < this.total}">{this.resources.length} displayed / </span>{this.total} contacts </div> <div class="row export"> <span>Export as:</span> <button class="button" onclick="{this.export}" format="xlsx">.xslx</button> <button class="button" onclick="{this.export}" format="csv">.csv</button> <button class="button" onclick="{this.copyMailchimp}">Mailchimp</button> <button class="button" onclick="{this.copy}">emails</button> </div> </div> <details if="{this.user.can(\'import\')}"> <summary>Actions on List</summary> <form class="row" onsubmit="{this.batchEditGroup}"> <span>Batch edit groups</span> <select name="group"> <optgroup each="{category in this.groups}" label="{category.kind}"> <option each="{group in category.values}" riot-value="{group.pk}">{group.label}</option> </optgroup> </select> <button onclick="{this.setOperator}" value="pull">Remove from All</button> <select name="role"> <option each="{role in this.roles}" riot-value="{role}">{role}</option> </select> <input type="checkbox" name="is_active" checked value="1" id="is_active"><label for="is_active">Is active</label> <button onclick="{this.setOperator}" value="push">Apply to All</button> </form> <form class="row" onsubmit="{this.batchEditKeyword}"> <span>Batch edit keyword</span> <select name="keyword"> <optgroup each="{category in this.keywords}" label="{category.kind}"> <option each="{keyword in category.values}" riot-value="{keyword.pk}">{keyword.label} ({keyword.types.join(\', \')})</option> </optgroup> </select> <button onclick="{this.setOperator}" value="push">Apply to All</button> <button onclick="{this.setOperator}" value="pull">Remove from All</button> </form> <form class="row" onsubmit="{this.batchEditKind}"> <span>Batch edit kind</span> <select name="kind"> <option each="{kind in this.kinds}" riot-value="{kind.key}" class="{main: kind.is_main}"> {kind.is_main ? \'\' : \'&nbsp;• \'}{kind.value} </option> </select> <button onclick="{this.setOperator}" value="set">Apply to All</button> </form> <form class="row" onsubmit="{this.batchRemoveKind}"> <button>Remove Kinds for All</button> </form> <form class="row" onsubmit="{this.batchEditDisease}"> <span>Batch edit disease</span> <select-autocomplete url="/disease/complete?limit=10&q=" class="row" name="disease" placeholder="Name of the disease"></select-autocomplete> <button onclick="{this.setOperator}" value="push">Apply to All</button> <button onclick="{this.setOperator}" value="pull">Remove from All</button> </form> </details> <section class="row"> <form onsubmit="{this.batchImportReferences}"> <details name="kind"> <summary>Find and Add Organisations by Emails or IDs</summary> <input type="hidden" name="resource" value="organisation"> <textarea name="references" placeholder="Paste your emails or ids of organisations"></textarea> <button>Find and Add</button> </details> </form> <form onsubmit="{this.batchImportReferences}"> <details name="kind"> <summary>Find and Add Contacts by Emails or IDs</summary> <input type="hidden" name="resource" value="person"> <textarea name="references" placeholder="Paste your emails or ids of contacts"></textarea> <button>Find and Add</button> </details> </form> </section> <div class="reference-error" if="{this.referencesNotFound.length}"> <span>The following {this.referencesNotFound.length} references could not be found:</span> <ul> <li each="{referenceNotFound in this.referencesNotFound}">{referenceNotFound}</li> </ul> </div> <div if="{this.errors && this.errors.length}" class="import-error"> <h3 class="error"> <icon figure="message-alert-small"></icon> Bulk editing could not apply on some organisations or contacts: </h3> <ul> <li each="{error in this.errors}"> <span if="{error.raw.organisation}">organisation {error.raw.organisation}</span> <span if="{Object.keys(error.raw).length > 1}"> / </span> <span if="{error.raw.person}">contact {error.raw.person}</span>: {error.error} </li> </ul> </div> </header> <table> <tr> <th>Organisation</th> <th>Name</th> <th>Contact</th> <th></th> </tr> <tr each="{resource, i in this.resources}"> <td><span if="{resource.organisation}"><a href="/organisation/{resource.organisation_pk}{location.search}">{resource.organisation}</a></span></td> <td> <span if="{resource.person_pk}"><a href="/person/{resource.person_pk}{location.search}">{resource.first_name} {resource.last_name}</a></span> </td> <td><span> <a if="{resource.email}" target="_blank" href="mailto:{resource.email}" target="_blank">{resource.email}</a> </span></td> <td><icon figure="delete" onclick="{this.remove}" riot-value="{i}" title="Remove this contact"></icon></td> </tr> </table> <a class="show-all" if="{this.resources && this.resources.length < this.total}" href="{replaceQueryParams({ limit: \'false\' })}">Show {this.total - this.limit} other results (slow)</a> <p if="{!this.resources || !this.resources.length}">No contact was added for export.</p>', 'export-list header,[data-is="export-list"] header{ padding-bottom: 1.2rem; margin-bottom: 1.6rem; color: var(--primary-color); border-bottom: 1px solid var(--border-color); } export-list header > .row,[data-is="export-list"] header > .row{ margin: 0; align-items: center; justify-content: space-between; } export-list h1,[data-is="export-list"] h1{ margin-bottom: 0; font-size: 2rem; font-weight: 300; } export-list [figure=delete],[data-is="export-list"] [figure=delete]{ cursor: pointer; } export-list table,[data-is="export-list"] table{ width: 100%; table-layout:fixed; counter-reset: section; } export-list th,[data-is="export-list"] th,export-list tr:first-child:before,[data-is="export-list"] tr:first-child:before{ color: rgba(80,80,110,0.6); } export-list th,[data-is="export-list"] th{ font-weight: normal; text-align: left; } export-list th:not(:last-child),[data-is="export-list"] th:not(:last-child),export-list td:not(:last-child),[data-is="export-list"] td:not(:last-child){ width: 31.5%; padding-right: 2rem; } export-list ol,[data-is="export-list"] ol{ list-style-position: inside; padding-left: 0; } export-list td,[data-is="export-list"] td{ padding: .1rem .5rem .1rem 0; } export-list td:first-of-type,[data-is="export-list"] td:first-of-type{ font-weight: 500; } export-list td a,[data-is="export-list"] td a{ display: inline-block; max-width: 98%; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; color: #50506E ; } export-list td > icon,[data-is="export-list"] td > icon{ opacity: 0.2; } export-list td > icon:hover,[data-is="export-list"] td > icon:hover{ opacity: 1; } export-list tr,[data-is="export-list"] tr{ height: 2rem; } export-list tr:not(:first-child):before,[data-is="export-list"] tr:not(:first-child):before,export-list tr:first-child:before,[data-is="export-list"] tr:first-child:before{ display: inline-block; width: 1.6rem; padding-right: 0.4rem; vertical-align: sub; color: #696E8C; } export-list tr:not(:first-child):before,[data-is="export-list"] tr:not(:first-child):before{ counter-increment: section; content: counter(section); } export-list tr:first-child:before,[data-is="export-list"] tr:first-child:before{ content: "#"; } export-list table a,[data-is="export-list"] table a{ text-decoration: none; border-bottom: none; word-break: break-all; } export-list td icon,[data-is="export-list"] td icon{ margin-right: .3rem; } export-list .results-number,[data-is="export-list"] .results-number{ font-weight: 300; } export-list .export,[data-is="export-list"] .export{ font-size: 0.6rem; } export-list .export span,[data-is="export-list"] .export span{ font-weight: 500; } export-list .show-all,[data-is="export-list"] .show-all{ text-align: center; display: block; } export-list details,[data-is="export-list"] details{ width: 100%; } export-list summary,[data-is="export-list"] summary{ font-size: inherit; } export-list textarea,[data-is="export-list"] textarea{ width: 80%; height: 100px; margin: 1rem 0; } export-list .reference-error,[data-is="export-list"] .reference-error{ background-color: #c72a2a; margin: 1rem 0 0 0; padding: 1rem; color: white; font-weight: bold; } export-list .reference-error ul,[data-is="export-list"] .reference-error ul{ list-style-type: none; padding-left: 0; } export-list .row form,[data-is="export-list"] .row form{ align-self: flex-start; } export-list form,[data-is="export-list"] form{ color: #000; } export-list option.main,[data-is="export-list"] option.main{ font-weight: bold; }', '', function(opts) {
    this.resources = []
    this.user = window.user
    this.operator = ''
    this.referencesNotFound = ''

    this.on('mount', async () => {
      let response = await api('/group')
      const groups = response.data.data
      this.groups = classifyByKind(groups)
      response = await api('/keyword')
      const keywords = response.data.data
      this.keywords = classifyByKind(keywords)
      this.kinds = config.ORGANISATION_KIND.map(k => {
        k.is_main = String(k.key).endsWith('0')
        return k
      })
      this.update()
    })

    this.on('*', () => {
      this.total = this.opts.resources && this.opts.resources.length
      this.limit = this.opts.limit || 100
      this.resourcesAll = this.opts.resources
      this.resources = this.opts.resources && this.opts.resources.slice(0, this.limit)
      this.roles = []
      const allroles = config.PERSON_TO_GROUP_ROLES
      allroles.forEach(g => {
        if(!this.roles.find(k => this.roles[k])) this.roles.unshift(g)
      })
    })

    this.setOperator = (event) => {
      this.operator = event.target.value
    }

    this.export = async (event) => {
      const uname = (new Date()).toLocaleString().replace(/\W+/g, '-')
      const format = event.currentTarget.getAttribute('format')
      const filename = `export-${uname}.${format}`
      data = { resources: exportList.all() }
      const response = apiResponse(await api(`/contact/export.${format}`, { data }))
      if (response.ok) download(filename, response.data, response.headers)
    }

    this.remove = (event) => {
      const index = event.currentTarget.value
      exportList.removeIndex(index)
      updatePage()
    }

    this.clear = () => {
      if (!confirm("Are you sure you want to remove all search results?")) return
      exportList.clear()
      updatePage()
    }

    this.copy = () => {
      navigator.clipboard.writeText(this.resourcesAll.filter(r => r.email).map(r => {
        return r.first_name && r.last_name ? `${r.first_name} ${r.last_name}<${r.email}>` : r.email
      }).join(';')).then(function() {
        notify('Emails are now copied!', 'info')
      }, function() {
        notify('Unable to copy emails, sorry!', 'error')
      })
    }

    this.copyMailchimp = () => {
      navigator.clipboard.writeText(this.resourcesAll.filter(r => r.email).map(r => r.email).join('\n')).then(
        () => notify('Ready to paste into Mailchimp!', 'info'),
        () => notify('Unable to copy emails, sorry!', 'error')
      )
    }

    this.batchEditGroup = async (event) => {
      event.preventDefault()
      const form = event.target
      const data = {
        resources: exportList.all(),
        field: 'groups',
        operator: this.operator,
        value: { group: form.group.value, role: form.role.value, is_active: form.is_active.checked }
      }
      const response = apiResponse(await api(`/basket`, { method: 'PATCH', data }))
      if(!response.ok) return
      this.errors = response.data.filter(item => item.status === 'error') || []
      notify('Batch was applied. The list of errors is displayed if any.', 'success')
      this.update()
    }

    this.batchEditKeyword = async (event) => {
      event.preventDefault()
      const form = event.target
      const data = {
        resources: exportList.all(),
        field: 'keywords',
        operator: this.operator,
        value: form.keyword.value
      }
      const response = apiResponse(await api(`/basket`, { method: 'PATCH', data }))
      if (!response.ok) return
      this.errors = response.data.filter(item => item.status === 'error') || []
      notify('Batch was applied. The list of errors is displayed if any.', 'success')
      this.update()
    }

    this.batchEditKind = async (event) => {
      event.preventDefault()
      const form = event.target
      const data = {
        resources: exportList.all(),
        field: 'kind',
        operator: this.operator,
        value: form.kind.value
      }
      const response = apiResponse(await api(`/basket`, { method: 'PATCH', data }))
      if (!response.ok) return
      this.errors = response.data.filter(item => item.status === 'error') || []
      notify('Batch was applied. The list of errors is displayed if any.', 'success')
      this.update()
    }

    this.batchRemoveKind = async (event) => {
      event.preventDefault()
      const data = {
        resources: exportList.all(),
        field: 'kind',
        operator: 'set',
        value: null
      }
      const response = apiResponse(await api(`/basket`, { method: 'PATCH', data }))
      if (!response.ok) return
      this.errors = response.data.filter(item => item.status === 'error') || []
      notify('Removed kinds for all. The list of errors is displayed if any.', 'success')
      this.update()
    }

    this.batchEditDisease = async (event) => {
      event.preventDefault()
      const form = event.target
      const data = {
        resources: exportList.all(),
        field: 'diseases',
        operator: this.operator,
        value: Number(form.querySelector('[name=disease]').value)
      }
      const response = apiResponse(await api(`/basket`, { method: 'PATCH', data }))
      if (!response.ok) return
      this.errors = response.data.filter(item => item.status === 'error') || []
      notify('Batch was applied. The list of errors is displayed if any.', 'success')
      this.update()
    }

    this.batchImportReferences = async (event) => {
      event.preventDefault()
      const resource = event.target.resource.value
      const rawRefs = event.target.references.value
      const refs = rawRefs.split(/\s+|,/g).map(d => d.trim()).filter(d => d)
      const response = apiResponse(await api('/basket', { method: 'PUT', data: { refs, resource } }))
      if(!response.ok) return
      exportList.addMany(response.data.resources)
      this.referencesNotFound = response.data.not_found
    }
});

riot.tag2('facet-check', '<button class="{status: true, selected: this.option}"><icon figure="{this.icon}"></icon></button> <div class="selector"> <button class="{selected: this.option}" onclick="{this.option ? this.deselect : this.select}" value="" title="Results can include this filter"> <icon figure="{this.icon}"></icon> </button> <button if="{this.option != true}" value="" onclick="{this.select}" title="Results may include this filter"> <icon figure="{this.icons[\'true\']}-{this.iconType}"> </button> <button if="{this.option != \'+\'}" value="+" onclick="{this.select}" title="Results must include this filter"> <icon figure="{this.icons[\'+\']}-{this.iconType}"> </button> <button if="{this.option != \'-\'}" value="-" onclick="{this.select}" title="Results must not include this filter"> <icon figure="{this.icons[\'-\']}-{this.iconType}"> </button> </div>', 'facet-check,[data-is="facet-check"]{ position: relative; height: 1em; width: 1em; display: flex; justify-content: center; align-items: center; } facet-check button.status + div,[data-is="facet-check"] button.status + div{ display: none; } facet-check button.status:hover + div,[data-is="facet-check"] button.status:hover + div,facet-check button.status + div:hover,[data-is="facet-check"] button.status + div:hover{ display: flex; } facet-check .selector,[data-is="facet-check"] .selector{ padding: 0.2rem 0.4rem; border-radius: 0.7rem; background-color: #fff; box-shadow: 0 3px 5px rgb(204, 204, 204, 0.5); margin: 0; width: min-content; position: absolute; top: -0.2rem; left: -0.4rem; z-index: 1000; display: flex; } facet-check .selector button:not(:last-child),[data-is="facet-check"] .selector button:not(:last-child){ margin-right: 0.3rem !important; }', '', function(opts) {
    const iconTypes = {
      'group': ['group_role', 'membership', 'group_kind'],
      'keyword': ['keyword_kind'],
      'type': ['group_kind', 'event_kind', 'event_role', 'event'],
    }
    this.icon = 'check-white'
    this.iconType = 'white'
    this.option = ''
    this.icons = {
      '+': 'plus',
      '-': 'outside',
      'true': 'check',
      '': 'checked',
      '<': 'check',
      '>': 'check',
    }

    this.on('*', () => {
      this.option = this.opts.option
      this.iconType = Object.keys(iconTypes).find(k => iconTypes[k].includes(this.opts.facetName)) || this.opts.facetName
      if(this.option) this.icon = `${this.icons[this.option]}-white`
    })

    this.select = (event) => {
      const modifier = event.currentTarget.value
      const value = createvalue(this.opts.riotValue, modifier)
      this.opts.onchange({
        action: 'add',
        category: this.opts.facetName,
        value,
        label: this.opts.label
      })
    }

    this.deselect = () => {
      this.opts.onchange({
        action: 'remove',
        category: this.opts.facetName,
        value: this.opts.riotValue,
      })
    }
});

riot.tag2('facets-details', '<div> <section if="{!this.hasFacets}"> <p> Your search is too narrow and has no result. Please remove some filter to add any other. </p> </section> <section each="{facet in this.facets}" if="{this.hasFacets && facet.inList !== false}" class="facetgroup {facet.name} {this.expandedFacets.includes(facet.name) ? \'expanded\' : \'\'}"> <div class="facetheader"> <h3><icon figure="{facet.name}"></icon>{facet.title}</h3> <div class="state-filter-radios" if="{facet.modifiers}"> <div> <input onclick="{this.toggleStatusFilter}" type="radio" id="{facet.name}-active" name="{facet.name}-status" category="{facet.name}" value="" checked="{facet.status == \'\'}"> <label for="{facet.name}-active">Active</label> </div> <div> <input onclick="{this.toggleStatusFilter}" type="radio" id="{facet.name}-inactive" name="{facet.name}-status" category="{facet.name}" value="<" checked="{facet.status === \'<\'}"> <label for="{facet.name}-inactive">Inactive</label> </div> <div> <input onclick="{this.toggleStatusFilter}" type="radio" id="{facet.name}-all" name="{facet.name}-status" category="{facet.name}" value="*" checked="{facet.status === \'*\'}"> <label for="{facet.name}-all">All</label> </div> </div> </div> <div class="facetcontent" if="{facet.values.length}"> <div each="{value in facet.values}" class="row facet"> <facet-check riot-value="{value.pk}" facet-name="{facet.name}" onchange="{this.addFilter}" label="{value.label}" option="{this.selectedFilter(facet.name, value.pk)}"></facet-check> <div> <icon if="{value.icon === ⁗ascendants⁗ || value.icon === ⁗descendants⁗}" figure="{value.icon}"></icon> <disease-gauge depth="{value.depth}" max-depth="{value.max_depth}"></disease-gauge> <span class="value-label" title="{value.label}">{value.label}</span> ({value.count}) </div> </div> <small if="{facet.values.length === 20}">(Displaying only the first 20 options)</small> </div> <div if="{!facet.values.length}"> There\'s no result for this facet </div> <button if="{facet.values.length > 10}" onclick="{this.toggleDisplay}" riot-value="{facet.name}"> <virtual if="{!this.expandedFacets.includes(facet.name)}"> Display more results </virtual> <virtual if="{this.expandedFacets.includes(facet.name)}"> Display less results </virtual> </button> </section> </div> <div class="toolbar"> <button class="button" onclick="{this.submit}"> <icon figure="filter" embedded></icon> Filter </button> </div>', 'facets-details > div,[data-is="facets-details"] > div{ display: grid; grid-column-gap: 3rem; grid-template-columns: repeat( 3, 30% ); } facets-details .facetgroup,[data-is="facets-details"] .facetgroup{ position: relative; margin-bottom: 1.6rem; } facets-details .facetgroup:after,[data-is="facets-details"] .facetgroup:after{ content: \'\'; position: absolute; bottom: 0; left: 0; display: block; width: 3.6rem; height: 1px; background-color: #E6E6EB; } facets-details .facetheader,[data-is="facets-details"] .facetheader{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: center; } facets-details .state-filter-radios,[data-is="facets-details"] .state-filter-radios{ display: flex; margin-top: -1rem; } facets-details .state-filter-radios > div:not(:first-child):not(:last-child),[data-is="facets-details"] .state-filter-radios > div:not(:first-child):not(:last-child){ border-right: 1px solid var(--border-color); border-left: 1px solid var(--border-color); } facets-details .state-filter-radios input,[data-is="facets-details"] .state-filter-radios input{ display: none; } facets-details .state-filter-radios label,[data-is="facets-details"] .state-filter-radios label{ margin: 0 .4rem; font-weight: normal; text-transform: none; color: var(--group); padding: 0 .5rem; border-radius: .5rem; font-size: .6rem; cursor: pointer; } facets-details .state-filter-radios div:last-child label,[data-is="facets-details"] .state-filter-radios div:last-child label{ margin-right: 0; } facets-details .state-filter-radios input:checked + label,[data-is="facets-details"] .state-filter-radios input:checked + label,facets-details .state-filter-radios label:hover,[data-is="facets-details"] .state-filter-radios label:hover{ color: #fff; background-color: var(--group); } facets-details .facetcontent,[data-is="facets-details"] .facetcontent{ max-height: 12.6rem; margin-bottom: 1.6rem; overflow: hidden; } facets-details .facetcontent .facet,[data-is="facets-details"] .facetcontent .facet{ color: var(--text-color) !important; } facets-details .facetgroup > div + button,[data-is="facets-details"] .facetgroup > div + button{ display: block; width: 100%; height: auto; margin-top: -1rem; margin-bottom: 1.6rem; text-align: left; background: none; border: none; } facets-details .facetgroup.expanded > div,[data-is="facets-details"] .facetgroup.expanded > div{ max-height: 100%; } facets-details .facetgroup p,[data-is="facets-details"] .facetgroup p{ margin: 0; font-style: italic; } facets-details .facet,[data-is="facets-details"] .facet{ height: 1.2rem; margin: 0 auto .4rem; color: inherit !important; } facets-details button.selected,[data-is="facets-details"] button.selected{ background-color: var(--primary-color); color: #fff; } facets-details button.selected img,[data-is="facets-details"] button.selected img{ width: .5rem; } facets-details button [figure=outsidebox-check-country] img,[data-is="facets-details"] button [figure=outsidebox-check-country] img,facets-details button [figure=outsidebox-uncheck-country] img,[data-is="facets-details"] button [figure=outsidebox-uncheck-country] img{ width: .7rem; } facets-details h3 icon,[data-is="facets-details"] h3 icon{ margin-right: 0.6rem; } facets-details h3 icon img,[data-is="facets-details"] h3 icon img{ width: 1.6rem; } facets-details icon[figure^=gauge],[data-is="facets-details"] icon[figure^=gauge]{ margin-right: 0; } facets-details icon[figure^=gauge] img,[data-is="facets-details"] icon[figure^=gauge] img{ width: 1.2rem; } facets-details [figure=ascendants] img,[data-is="facets-details"] [figure=ascendants] img,facets-details [figure=descendants] img,[data-is="facets-details"] [figure=descendants] img{ width: 1rem; margin-right: 0.2rem; } facets-details h3,[data-is="facets-details"] h3{ display: flex; align-items: center; margin-bottom: 1rem; } facets-details .disease h3,[data-is="facets-details"] .disease h3{ color: var(--disease) } facets-details .disease button,[data-is="facets-details"] .disease button{ border-color: var(--disease); } facets-details .disease button.selected,[data-is="facets-details"] .disease button.selected{ background-color: var(--disease); } facets-details .country h3,[data-is="facets-details"] .country h3{ color: var(--country) } facets-details .country button,[data-is="facets-details"] .country button{ border-color: var(--country); } facets-details .country button.selected,[data-is="facets-details"] .country button.selected{ background-color: var(--country); } facets-details .keyword h3,[data-is="facets-details"] .keyword h3,facets-details .keyword_kind h3,[data-is="facets-details"] .keyword_kind h3{ color: var(--keyword) } facets-details .keyword button,[data-is="facets-details"] .keyword button,facets-details .keyword_kind button,[data-is="facets-details"] .keyword_kind button{ border-color: var(--keyword); } facets-details .keyword button.selected,[data-is="facets-details"] .keyword button.selected,facets-details .keyword_kind button.selected,[data-is="facets-details"] .keyword_kind button.selected{ background-color: var(--keyword); } facets-details .grouping h3,[data-is="facets-details"] .grouping h3{ color: var(--grouping) } facets-details .grouping button,[data-is="facets-details"] .grouping button{ border-color: var(--grouping); } facets-details .grouping button.selected,[data-is="facets-details"] .grouping button.selected{ background-color: var(--grouping); } facets-details .kind h3,[data-is="facets-details"] .kind h3{ color: var(--kind) } facets-details .kind button,[data-is="facets-details"] .kind button{ border-color: var(--kind); } facets-details .kind button.selected,[data-is="facets-details"] .kind button.selected{ background-color: var(--kind); } facets-details .group h3,[data-is="facets-details"] .group h3,facets-details .group_role h3,[data-is="facets-details"] .group_role h3,facets-details .group_kind h3,[data-is="facets-details"] .group_kind h3,facets-details .membership h3,[data-is="facets-details"] .membership h3{ color: var(--group) } facets-details .group button,[data-is="facets-details"] .group button,facets-details .group_role button,[data-is="facets-details"] .group_role button,facets-details .group_kind button,[data-is="facets-details"] .group_kind button,facets-details .membership button,[data-is="facets-details"] .membership button{ border-color: var(--group); } facets-details .group button.selected,[data-is="facets-details"] .group button.selected,facets-details .group_role button.selected,[data-is="facets-details"] .group_role button.selected,facets-details .group_kind button.selected,[data-is="facets-details"] .group_kind button.selected,facets-details .membership button.selected,[data-is="facets-details"] .membership button.selected{ background-color: var(--group); } facets-details :not(.toolbar) button,[data-is="facets-details"] :not(.toolbar) button{ width: .8rem; height: .8rem; padding: 0; border-radius: .1rem; } facets-details :not(.toolbar) button icon,[data-is="facets-details"] :not(.toolbar) button icon{ display: flex; justify-content: center; align-items: center; height: 0.75rem; margin-right: 0; } facets-details facet-check + div,[data-is="facets-details"] facet-check + div{ display: flex; width: calc(100% - 2.5rem); } facets-details button + div,[data-is="facets-details"] button + div{ width: calc(100% - 1.2rem); margin-left: .2rem; } facets-details .buttongroup,[data-is="facets-details"] .buttongroup{ margin-right: .4rem !important; } facets-details .value-label,[data-is="facets-details"] .value-label{ width: 100%; margin-right: 0.4rem; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; cursor: help; } facets-details .toolbar,[data-is="facets-details"] .toolbar{ justify-content: flex-end; width: calc(100% + 1rem); } facets-details .keyword,[data-is="facets-details"] .keyword{ display: block; }', '', function(opts) {
    this.facets = []
    this.filters = []
    this.hasFacets = false
    this.expandedFacets = []

    this.on('*', () => {
      this.filters = this.opts.filters

      if(this.opts.facets) {
        this.facets = window.facetSettings.reduce((acc, f) => {
          const values = this.opts.facets[f.name]
          const filter = this.filters.find(filter => filter.name == f.name)
          const facet = this.facets.find(facet => facet.name == f.name) || {}
          let status = facet.status
          if(status === undefined && filter) status = getvalue(filter.value).status
          acc.push(Object.assign(f, { values, status }))
          return acc
        }, [])

        this.hasFacets = this.facets.find(f => f.values && f.values.length)
      }
    })

    this.selectedFilter = (category, value) => {
      if(!value) value = ''
      const filter = this.filters.find(f => f.name == category && getvalue(f.value).value == value)

      if(!filter) return
      return getvalue(filter.value).modifier || true
    }

    this.addFilter = (data) => {
      const filter = this.filters.find(f => f.name === data.category && getvalue(f.value).value === getvalue(data.value).value)
      if(filter) this.filters.splice(this.filters.indexOf(filter), 1)
      if(data.action === 'add')
        this.filters.push({ name: data.category, value: data.value, label: data.label })
    }

    this.toggleStatusFilter = (event) => {
      const selected = event.target
      const facet = this.facets.find(f => f.name == selected.getAttribute('category'))
      const index = this.facets.indexOf(facet)
      facet.status = selected.getAttribute('value')
      this.facets[index] = facet
    }

    this.submit = () => {
      const groups = this.filters.reduce((groups, f) => {
        if(!(f.name in groups)) groups[f.name] = []
        const facet = this.facets.find(facet => facet.name == f.name)
        const status = facet && facet.status
        const value = createvalue(f.value, null, status)
        groups[f.name].push(`${value}|${f.label}`)
        return groups
      }, {})
      groups.showFacets = true
      page(replaceQueryParams(groups, false))
    }

    this.toggleDisplay = (event) => {
      const name = event.target.value
      this.expandedFacets.includes(name)
        ? this.expandedFacets.pop(name)
        : this.expandedFacets.push(name)
    }

});

riot.tag2('filters', '<div class="row"> <button hide="{this.opts.facetsVisible}" onclick="{this.opts.toggle}" class="toggle button"><icon figure="view" embedded></icon> View all filters</button> <button show="{this.opts.facetsVisible}" onclick="{this.opts.toggle}" class="toggle button"><icon figure="viewnone" embedded></icon> Close filters</button> <virtual each="{group in this.groupedFilters}"> <span each="{filter in group.filters}" class="{\'tag-part\': true, exclude: getvalue(filter.value).modifier == \'-\', include: getvalue(filter.value).modifier == \'+\', tag: true}" title="Filtering by {group.name} matching \'{filter.label}\'"> <span class="{classForFilter(group.name)}"> <icon figure="{filter.icon}"></icon> {filter.label}&nbsp; <span if="{filter.facet?.modifiers && getvalue(filter.value).status == \'\'}">| Active</span> <span if="{filter.facet?.modifiers && getvalue(filter.value).status == \'<\'}">| Inactive</span> <span if="{filter.facet?.modifiers && getvalue(filter.value).status == \'*\'}">| All</span> <button class="plain" onclick="{this.removeFilter}" name="{filter.name}" riot-value="{filter.value}"> <icon figure="close" embedded></icon> </button> </span> </span> </virtual> </div>', 'filters,[data-is="filters"]{ display: block; } filters > .row,[data-is="filters"] > .row{ flex-flow: wrap; align-items: end; margin: 0; } filters button,[data-is="filters"] button{ height: 1.6rem; margin-bottom: 1rem; } filters icon img,[data-is="filters"] icon img{ min-width: .8rem; } filters .tag,[data-is="filters"] .tag{ display: flex; margin-right: 0 !important; color: #0464ba; font-size: .6rem; font-weight: 500; } filters .tag button,[data-is="filters"] .tag button{ margin-bottom: 0; margin-left: 0.4rem; padding: 0; width: .8rem; height: .8rem; } filters .tag-part > span,[data-is="filters"] .tag-part > span{ display: flex; align-items: center; margin-right: .8rem; margin-bottom: 1rem; padding: .2rem .6rem ; background-color: #e0eaf3; border-radius: 1rem; cursor: help; } filters .tag-part icon:first-of-type,[data-is="filters"] .tag-part icon:first-of-type{ margin-right: 0.4rem; } filters .tag-part .disease,[data-is="filters"] .tag-part .disease{ color: var(--disease); background-color: var(--disease-light); } filters .tag-part .disease button,[data-is="filters"] .tag-part .disease button{ color: var(--disease); } filters .tag-part .country,[data-is="filters"] .tag-part .country{ color: var(--country); background-color: var(--country-light); } filters .tag-part .country button,[data-is="filters"] .tag-part .country button{ color: var(--country); } filters .tag-part .group,[data-is="filters"] .tag-part .group,filters .tag-part .group_role,[data-is="filters"] .tag-part .group_role,filters .tag-part .membership,[data-is="filters"] .tag-part .membership{ color: var(--group); background-color: var(--group-light); } filters .tag-part .group button,[data-is="filters"] .tag-part .group button,filters .tag-part .group_role button,[data-is="filters"] .tag-part .group_role button,filters .tag-part .membership button,[data-is="filters"] .tag-part .membership button{ color: var(--group); } filters .tag-part .keyword,[data-is="filters"] .tag-part .keyword{ color: var(--keyword); background-color: var(--keyword-light); } filters .tag-part .keyword button,[data-is="filters"] .tag-part .keyword button{ color: var(--keyword); } filters .tag-part .kind,[data-is="filters"] .tag-part .kind{ color: var(--kind); background-color: var(--kind-light); } filters .tag-part .kind button,[data-is="filters"] .tag-part .kind button{ color: var(--kind); } filters .tag-part .grouping,[data-is="filters"] .tag-part .grouping{ color: var(--grouping); background-color: var(--grouping-light); } filters .tag-part .grouping button,[data-is="filters"] .tag-part .grouping button{ color: var(--grouping); } filters .tag-part.exclude,[data-is="filters"] .tag-part.exclude{ text-decoration: line-through; } filters .tag-part.include,[data-is="filters"] .tag-part.include{ font-weight: bold; } filters [figure=close] svg path,[data-is="filters"] [figure=close] svg path{ stroke: var(--primary-color); } filters .disease [figure=close] svg path,[data-is="filters"] .disease [figure=close] svg path{ stroke: var(--disease); } filters .grouping [figure=close] svg path,[data-is="filters"] .grouping [figure=close] svg path{ stroke: var(--grouping); } filters .group [figure=close] svg path,[data-is="filters"] .group [figure=close] svg path,filters .membership [figure=close] svg path,[data-is="filters"] .membership [figure=close] svg path,filters .group_role [figure=close] svg path,[data-is="filters"] .group_role [figure=close] svg path{ stroke: var(--group); } filters .keyword [figure=close] svg path,[data-is="filters"] .keyword [figure=close] svg path{ stroke: var(--keyword); } filters .kind [figure=close] svg path,[data-is="filters"] .kind [figure=close] svg path{ stroke: var(--kind); }', '', function(opts) {
    this.facetSettings = window.facetSettings

    this.on('*', () => {
      this.filters = this.opts.filters

      this.filters = this.filters.map((f) => {
        f.icon = f.name.toLowerCase()
        const status = getvalue(f.value).status
        if (f.icon == 'disease' && ['<', '>'].includes(status)) {
          f.icon = (status === '>') ? 'descendants' : 'ascendants'
        }
        f.label = f.label || f.value
        return f
      })

      this.filters = this.filters.filter(f => f.name !== 'resource')

      this.facets = this.opts.facets && Object.keys(this.opts.facets).map(key => ({
        name: key,
        values: this.opts.facets[key]
      }))

      this.groupedFilters = window.facetSettings.reduce((acc, facet) => {
        const filters = this.filters.filter(f => f.name === facet.name)
        if(filters.length) acc.push(Object.assign(facet, { filters }))
        return acc
      }, [])
    })

    this.removeFilter = (event) => {
      const { name, value } = event.currentTarget
      const facets = this.filters.filter(f => f.name === name && f.value !== value)
      page(replaceQueryParams({ [name]: facets.map(f => {
        return f.label ? `${f.value}|${f.label}` : f.value
      }) }))
    }
});

riot.tag2('grouping-tag', '<tag type="grouping" label="{this.label}" riot-value="{this.opts.riotValue}"></tag>', 'grouping-tag a,[data-is="grouping-tag"] a{ font-weight: 500; }', '', function(opts) {
  this.on("*",()=>{
    this.label = keyToValue(config.GROUPING, this.opts.riotValue)
  })
});

riot.tag2('history', '<h1>Recent changes</h1> <form onsubmit="{this.filter}" class="row"> <div> <label>Action</label> <select name="action" onchange="{this.actionChanged}"> <option value="">All</option> <option value="create">Created</option> <option value="update">Modified</option> <option value="delete">Deleted</option> </select> </div> <div if="{this.action == \'update\'}"> <label>Field concerned</label> <select name="field"> <option value="">All</option> <option value="cell_phone">cell phone</option> <option value="comment">comment</option> <option value="country">country</option> <option value="diseases">diseases</option> <option value="email">email</option> <option value="groupings">groupings</option> <option value="events">events</option> <option value="first_name">first name</option> <option value="groups">groups</option> <option value="keywords">keywords</option> <option value="languages">languages</option> <option value="last_name">last name</option> <option value="phone">phone</option> <option value="roles">roles</option> <option value="title">title</option> </select> </div> <button><icon figure="filter"></icon>Filter</button> </form> <div class="row"> <button onclick="{this.toggleCollapse}">Collapse / Extend All</button> <button onclick="{this.download}">Download in JSON</button> </div> <hr> <diffs diffs="{this.opts.changes}" withcontext="{true}" collapse="{this.collapse}"></diffs>', 'history form.row,[data-is="history"] form.row{ align-items: flex-end; }', '', function(opts) {
    this.action = ''
    this.collapse = true

    this.filter = (event) => {
      event.preventDefault()
      page(replaceQueryParams(serializeForm(event.target), false))
    }

    this.actionChanged = (event) => {
      this.action = event.target.value
    }

    this.toggleCollapse = () => {
      this.collapse = !this.collapse
    }

    this.download = async () => {
      const response = apiResponse(await api(replaceQueryParams({ limit: 1000 })))
      const filename = `log-of-logs.json`
      if (response.ok) download(filename, JSON.stringify(response.data), response.headers)
    }
});

riot.tag2('home', '<div riot-style="background-image: url(\'/src/images/background/{this.imageId}.jpg\')"></div>', 'home,[data-is="home"],home div,[data-is="home"] div{ display: block; } home div,[data-is="home"] div{ height: 100%; background-size: cover; background-repeat: no-repeat; background-position: center; }', '', function(opts) {
    this.imageId = Math.ceil(Math.random() * 14)
});


riot.tag2('icon', '<img riot-src="{this.url}"> <span if="{this.opts.label}">{this.opts.label}</span>', 'icon,[data-is="icon"]{ display: inline-flex; align-items: center; justify-content: center; } icon img,[data-is="icon"] img{ width: auto; vertical-align: middle; }', '', function(opts) {
    this.url = ''

    this.replaceContent = async () => {
      const request = await fetch(this.url)
      const response = await request.text()
      this.root.innerHTML = response
    }

    this.on('mount', () => {
      if(Object.keys(this.opts).includes('embedded')) this.replaceContent()
    })
    this.on('*', () => {
      this.url = `/src/images/icons/${ this.opts.figure }.svg`
    })
});

riot.tag2('login', '<div> <img src="/src/images/logo.svg" alt="EURORDIS Contact Database"> <h1>Login with your email</h1> <p>Enter your @eurordis.org email, then we send you a connection link.</p> <form onsubmit="{this.submit}"> <label for="email">Your @eurordis.org email</label> <div> <input id="email" name="email" placeholder="ex.: joe@eurordis.org" required type="email"> <button class="button"> <icon figure="email" embedded></icon> Send me a link</button> </div> </form> </div>', 'login > div,[data-is="login"] > div{ display: flex; flex-direction: column; justify-content: center; height: 100%; } login > div > img,[data-is="login"] > div > img{ width: 10.5rem; height: 3.2rem; margin-bottom: 4rem; } login h1,[data-is="login"] h1{ font-family: "IBMPlexSans", sans-serif; } login p,[data-is="login"] p{ margin-bottom: 0; color: var(--primary-color) } login form div,[data-is="login"] form div{ display: flex; align-items: center; } login label,[data-is="login"] label{ margin-top: 0; } login button,[data-is="login"] button{ margin-left: 1.6rem; }', '', function(opts) {
    this.submit = async (event) => {
      event.preventDefault()
      const email = event.target.email.value
      const response = apiResponse(await api('/token', { data: { email } }))
      if (!response.ok) return notify(response.data.error, 'error')
      localStorage.setItem('token', response.data)
      notify("Check your email to access the Contact Database!", 'success')
    }
});

riot.tag2('membership-fees', '<details if="{this.opts.fees && this.opts.fees.length}"> <summary>View fees</summary> <table> <tr each="{fee in this.opts.fees}"> <td> {fee.year}: <virtual if="{fee.exempted}">exempted</virtual> <virtual if="{!fee.exempted}"> {fee.amount}€ </virtual> </td> <td> <virtual if="{!fee.exempted}"> ({trans(new Date(fee.payment_date))}) <span if="{!fee.receipt_sent}"> — Receipt pending</span> </virtual> </td> </tr> </table> </details>', 'membership-fees td:first-of-type,[data-is="membership-fees"] td:first-of-type{ width: 6rem; }', '', function(opts) {
});
riot.tag2('organisation-details', '<header class="{row: true, deleted: this.deleted}"> <div> <div class="avatar"> <icon figure="placeholder-organisation"></icon> </div> </div> <div> <h1>{this.organisation.name} <small if="{this.organisation.acronym}">( {this.organisation.acronym} )</small><add-export organisation-pk="{this.organisation.pk}"></add-export></h1> <h2 if="{this.organisation.kind || this.organisation.keywords.length}"> <tag type="kind" label="{this.kind}"></tag> <tag each="{kw in this.organisation.keywords}" type="keyword" label="{kw.label}" riot-value="{kw.pk}"></tag> </h2> <div class="row"> <div> <ul> <li if="{this.organisation.address || this.organisation.postcode || this.organisation.city || this.organisation.country}" class="row"> <icon figure="address"></icon> <div> <div if="{this.organisation.address}">{this.organisation.address}</div> <span if="{this.organisation.postcode}">{this.organisation.postcode} </span> <span if="{this.organisation.city}"> {this.organisation.city} </span> <span if="{this.organisation.country}">({getCountry(this.organisation.country).label})</span> </div> </li> <li if="{this.organisation.email}"><icon figure="email"></icon><a target="_blank" href="mailto:{this.organisation.name}<{this.organisation.email}>">{this.organisation.email}</a></li> <li if="{this.organisation.website}"><icon figure="web"></icon><a href="{this.organisation.website}" target="_blank">{this.organisation.website}</a></li> <li if="{this.organisation.phone}"><icon figure="landline"></icon>{this.organisation.phone}</li> <li if="{this.organisation.cell_phone}"><icon figure="phone"></icon>{this.organisation.cell_phone}</li> <li if="{this.membersCount}"><icon figure="membership"></icon><a href="{location.pathname}?resource=organisation&membership={this.organisation.pk}|{this.organisation.label}">{this.membersCount} members</a></li> <li if="{this.votingContact}"><icon figure="votingcontact"></icon><a href="/person/{this.votingContact.person.pk}">{this.votingContact.person.label}</a></li> </ul> </div> <div> <div if="{this.organisation.groupings && this.organisation.groupings.length}" class="grouping"> <grouping-tag each="{grouping in this.opts.organisation.groupings}" riot-value="{grouping}"></grouping-tag> </div> <div if="{this.diseases.length <= 3}" class="diseases-list compact"> <tag each="{disease in this.diseases}" type="disease" label="{disease.name}" external-url="https://www.orpha.net/consor/cgi-bin/OC_Exp.php?Expert={disease.pk}" external-title="View details on Orphanet" riot-value="{disease.pk}" details-link="/disease/{disease.pk}"></tag> </div> </div> </div> </div> </header> <section if="{this.diseases.length > 3}" class="diseases-list"> <div> <h2 class="disease">Diseases</h2> <expandable-list items="{this.diseases}"> <yield> <tag each="{disease in this.opts.items.slice(0, this.slice)}" type="disease" label="{disease.name}" external-url="https://www.orpha.net/consor/cgi-bin/OC_Exp.php?Expert={disease.pk}" external-title="View details on Orphanet" riot-value="{disease.pk}" details-link="/disease/{disease.pk}"></tag> </yield> </expandable-list> </div> </section> <section if="{this.organisation.membership.length}"> <div> <h2 class="membership">Memberships</h2> <ul> <li each="{membership in this.activeMemberships}"> <a href="/organisation/{membership.organisation.pk}">{membership.organisation.label}</a><span if="{membership.status}">&nbsp;({keyToValue(this.membershipStatuses, membership.status)})</span> <span if="{membership.start_date}" class="from-date">&nbsp;From {trans(new Date(membership.start_date))}</span> <membership-fees fees="{membership.fees}"></membership-fees> </li> </ul> <virtual if="{this.previousMemberships.length}"> <details> <summary>Previous memberships ({this.previousMemberships.length})</summary> <table> <tr each="{membership in this.previousMemberships}"> <td> {membership.organisation.label} <span if="{membership.status}">({keyToValue(this.membershipStatuses, membership.status)})</span> <membership-fees fees="{membership.fees}"></membership-fees> </td> <td> <span if="{membership.start_date}">From {trans(new Date(membership.start_date))}</span> <span if="{membership.end_date}"> until {trans(new Date(membership.end_date))}</span> </td> <td> </td> </tr> </table> </details> </div> </section> <section if="{this.organisation.roles && this.organisation.roles.length}"> <div> <div class="contact-header"> <h2>Contacts</h2> <add-export roles="{this.organisation.roles}"></add-export> </div> <table> <tr> <th>Name</th> <th>Position</th> <th>Actions</th> </tr> <tr each="{role in this.organisation.roles}"> <td class="role-name-data"> <icon figure="{role.is_primary ? \'primary\' : \'person\'}"></icon> <country-flag country="{role.person.country}"></country-flag> <a href="/person/{role.person.pk}/{location.search}" title="View profile in details"> {role.person.first_name} {role.person.last_name} </a> </td> <td> {role.name} </td> <td> <add-export person-pk="{role.person.pk}" organisation-pk="{this.opts.organisation.pk}"></add-export> <a class="email" if="{role.email || role.person.email || this.organisation.email}" target="_blank" href="mailto:{role.person.label}<{role.email || role.person.email || this.organisation.email}>" title="Send an email"><icon figure="email"></icon></a> </td> </tr> </table> </div> </section> <section class="property-group" if="{this.groupsCount}"> <div> <h2 class="group">Groups</h2> <div each="{role in this.organisation.roles}" if="{role.activeGroups.length || role.inactiveGroups.length}" class="item"> <h3>{role.person.label}</h3> <div class="active" if="{role.activeGroups.length}"> Active: <p each="{group in role.activeGroups}"> <a href="/?group={group.group.pk}|{group.group.label}" title="{group.group.description}"> <icon figure="group"></icon> {group.group.label} </a> <span if="{group.role}"> {group.role}</span> </p> </div> <div class="inactive" if="{role.inactiveGroups.length}"> Inactive: <p each="{group in role.inactiveGroups}"> <a href="/?group={group.group.pk}|{group.group.label}" title="{group.group.description}"> <icon figure="group"></icon>{group.group.label}</a> <span if="{group.role}"> {group.role}</span> </p> </div> <div> </div> </section> <section if="{this.organisation.adherents || this.organisation.budget || this.organisation.creation_year || this.organisation.board || this.organisation.paid_staff || this.organisation.pharma_funding}"> <div> <h2>Patient Organisation Details</h2> <p> <virtual if="{this.organisation.adherents}">{this.organisation.adherents} members, </virtual> <virtual if="{this.organisation.budget}">annual budget of {Number(this.organisation.budget).toLocaleString()}€, </virtual> <virtual if="{this.organisation.creation_year}">since {this.organisation.creation_year}</virtual> </p> <p> <virtual if="{this.organisation.board}">{this.organisation.board} patient representative vs board director, </virtual> <virtual if="{this.organisation.paid_staff}">paid staff: {Number(this.organisation.paid_staff)}, </virtual> <virtual if="{this.organisation.pharma_funding}"> {this.organisation.pharma_funding}% funded by pharma.</virtual> </p> </div> </section> <section if="{this.organisation.attachments.length}"> <div> <h2>Documents</h2> <div each="{attachment in this.organisation.attachments}"> <attachment-link contact-id="{this.organisation.pk}" attachment="{attachment}"></attachment-link> </div> </div> </section> <section if="{this.organisation.comment}"> <div> <h2>Comments</h2> <p> <raw html="{this.organisation.comment.replace(/\\n/g, \'<br>\' )}"></raw> </p> </div> </section> <diffs if="{this.opts.diffs}" diffs="{this.opts.diffs}"></diffs> <section class="toolbar"> <small> id: {this.organisation.pk} <span if="{this.organisation.filemaker_pk}">(previously {this.organisation.filemaker_pk})</span> <p> Created on {trans(new Date(this.organisation.created_at))} <virtual if="{this.organisation.created_by}"> by <a href="/person/{this.organisation.created_by.pk}"> {this.organisation.created_by.first_name} {this.organisation.created_by.last_name} </a> </virtual> </p> <p> Modified on {trans(new Date(this.organisation.modified_at))} <virtual if="{this.organisation.modified_by}"> by <a href="/person/{this.organisation.modified_by.pk}"> {this.organisation.modified_by.first_name} {this.organisation.modified_by.last_name} </a> </virtual> </p> <a href="{getUrl(\'/organisation/\' + this.opts.organisation.pk + \'/history\')}">Blame who messed with this</a> </small> <div> <a class="button" if="{this.hasFees}" href="{getUrl(\'/organisation/\'+this.opts.organisation.pk+\'/edit/fees\')}" style="margin-right: .8rem"> <icon figure="fees-small" embedded></icon>Edit Fees </a> <a href="/organisation/{this.opts.organisation.pk}/edit/{location.search}" class="button"><icon figure="edit" embedded></icon> Edit this organisation</a> </div> </section> <link rel="stylesheet" href="/src/css/details.css">', 'organisation-details [figure=person] img,[data-is="organisation-details"] [figure=person] img,organisation-details country-flag img,[data-is="organisation-details"] country-flag img{ width: 1.2rem; } organisation-details [figure=basket] img,[data-is="organisation-details"] [figure=basket] img{ width: 1.2rem; } organisation-details [figure=person],[data-is="organisation-details"] [figure=person],organisation-details [figure=primary],[data-is="organisation-details"] [figure=primary]{ margin-right: .3rem; } organisation-details country-flag,[data-is="organisation-details"] country-flag{ margin-right: .4rem; } organisation-details h2 tag,[data-is="organisation-details"] h2 tag{ margin-right: 1.2rem; } organisation-details h1 small,[data-is="organisation-details"] h1 small{ font-size: 0.9rem; color: var(--text-color); } organisation-details h1 > *,[data-is="organisation-details"] h1 > *{ margin-top: .4rem; margin-left: .2rem; } organisation-details .email icon,[data-is="organisation-details"] .email icon{ height: 1rem; margin-left: .5rem; } organisation-details table,[data-is="organisation-details"] table{ width: 100%; } organisation-details table a,[data-is="organisation-details"] table a{ border-bottom: none; } organisation-details th,[data-is="organisation-details"] th{ text-align: left; color: rgba(80,80,110,0.6); font-weight: normal; } organisation-details td:last-child a,[data-is="organisation-details"] td:last-child a{ padding: 0; } organisation-details td:last-child a:hover,[data-is="organisation-details"] td:last-child a:hover{ background: none; } organisation-details .grouping,[data-is="organisation-details"] .grouping{ margin-top: .4rem; } organisation-details .membership + ul li,[data-is="organisation-details"] .membership + ul li{ display: block; } organisation-details membership-fees summary + ul,[data-is="organisation-details"] membership-fees summary + ul{ display: flex; flex-wrap: wrap } organisation-details membership-fees summary + ul li,[data-is="organisation-details"] membership-fees summary + ul li{ flex-basis: 33% } organisation-details .from-date,[data-is="organisation-details"] .from-date{ margin-left: 0.4rem; color: #696E8C; } organisation-details .deleted *,[data-is="organisation-details"] .deleted *,organisation-details .deleted ~ * *,[data-is="organisation-details"] .deleted ~ * *{ opacity: .8; } organisation-details .deleted h1,[data-is="organisation-details"] .deleted h1{ text-decoration: line-through; } organisation-details .role-name-data,[data-is="organisation-details"] .role-name-data{ display: flex; align-items: center; } organisation-details .contact-header,[data-is="organisation-details"] .contact-header{ display: flex; align-items: center; margin-bottom: .8rem; } organisation-details section .contact-header h2,[data-is="organisation-details"] section .contact-header h2{ margin-bottom: 0; } organisation-details .contact-header add-export,[data-is="organisation-details"] .contact-header add-export{ display: flex; align-items: center; margin-left: 0.5rem; } organisation-details .diseases-list,[data-is="organisation-details"] .diseases-list{ max-width: 100rem; } organisation-details tag[type=disease],[data-is="organisation-details"] tag[type=disease]{ margin-left: 0 !important; margin-right: 5%; width: 45%; } organisation-details .diseases-list.compact tag[type=disease],[data-is="organisation-details"] .diseases-list.compact tag[type=disease]{ width: 100%; } organisation-details .toolbar > div,[data-is="organisation-details"] .toolbar > div{ display: flex; }', 'class="details"', function(opts) {
    this.organisation = {}
    this.diseases = []
    this.deleted = false
    this.membershipStatuses = config.MEMBERSHIP_STATUSES
    this.membersCount = 0

    function isActiveMembership(membership) {
      return !membership.end_date && membership.status !== 'w'
    }

    this.on('*', async () => {
      const hasChanged = this.organisation !== this.opts.organisation
      this.organisation = this.opts.organisation
      this.diseases = this.organisation.diseases.sort((a,b) => a.name > b.name ? 1 : -1)
      this.deleted = this.organisation.keywords.find(k => k.name.toLowerCase() === 'to be deleted')
      this.organisation.keywords = this.organisation.keywords.sort((k1, k2) => k1.label.toLowerCase() < k2.label.toLowerCase() ? -1 : 1)

      this.activeMemberships = this.organisation.membership.filter(isActiveMembership)
      this.previousMemberships = this.organisation.membership.filter(m => !isActiveMembership(m))
      this.kind = keyToValue(config.ORGANISATION_KIND, this.organisation.kind)
      this.hasFees = this.organisation.membership.find(o => o.organisation.pk == config.EURORDIS_ORGANISATION.pk)
      this.groupsCount = this.organisation.roles.reduce((total, role) => total += role.groups.length, 0)
      this.votingContact = this.organisation.roles.find(r => r.is_voting)
      this.organisation.roles = this.organisation.roles.map(role => {
        role.activeGroups = role.groups.filter(g => g.is_active)
        role.inactiveGroups = role.groups.filter(g => !g.is_active)
        return role
      })

      if(hasChanged) {
        const response = apiResponse(await api(`/search?resource=organisation&membership=${this.organisation.pk}`))
        this.membersCount = response.data.total
        this.update()
      }
    })
});

riot.tag2('organisation-edit', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-organisation"></icon> </div> </div> <div> <h1 if="{organisation.pk}">Edit: {this.opts.organisation.name} <small if="{this.organisation.acronym}">( {this.organisation.acronym} )</small></h1> <h1 if="{!organisation.pk}">Add an organisation</h1> <h2 if="{this.organisation.kind}" class="kind"><icon figure="kind"></icon>{keyToValue(this.kinds, this.organisation.kind)} </h2> </div> </header> <form onsubmit="{this.save}" class="resource-edit-form"> <fieldset> <div> <div> <legend>Organisation Information</legend> <div class="row"> <div class="w63p"> <label for="name">Name in local language / Name in English</label> <input name="name" riot-value="{this.organisation.name}" placeholder="Name in local language / name in English" required onblur="{this.checkDuplicatesName}"> </div> <div> <label for="acronym">Acronym</label> <input name="acronym" riot-value="{this.organisation.acronym}" placeholder="Acronym"> </div> </div> <duplicates href="{this.duplicatesNameUri}" resource="organisation"></duplicates> <label for="website">Website</label> <input type="url" name="website" riot-value="{this.organisation.website}" placeholder="Website (type http://…)" class="w63p" onblur="{this.checkDuplicatesUrl}"> <duplicates href="{this.duplicatesUrlUri}" resource="organisation"></duplicates> <label for="kind">Type</label> <select name="kind" class="w63p"> <option selected="{this.organisation.kind == \'\'}" value=""> — Kind —</option> <option each="{kind in this.kinds}" riot-value="{kind.key}" class="{main: kind.is_main}" selected="{this.organisation.kind == kind.key}"> {kind.is_main ? \'\' : \'&nbsp;• \'}{kind.value} </option> </select> </div> </div> </fieldset> <fieldset> <div> <div> <legend>Address</legend> <label for="address">Address</label> <input name="address" riot-value="{this.organisation.address}" placeholder="Address"> <div class="row"> <div class="w20p"> <label for="postcode">Postcode</label> <input name="postcode" riot-value="{this.organisation.postcode}" placeholder="Postal code"> </div> <div class="fx-grow-2"> <label for="city">City</label> <input name="city" riot-value="{this.organisation.city}" placeholder="City"> </div> <div> <label for="country">Country</label> <select data-is="select-country" country="{this.organisation.country}" name="country" onchange="{this.changeCountry}"></select> </div> </div> </div> </div> </fieldset> <fieldset class="w70p"> <div> <div> <legend>Contact Details</legend> <div class="row"> <div class="w50p"> <label for="cell_phone">Cellphone</label> <input name="cell_phone" riot-value="{this.organisation.cell_phone}" placeholder="Mobile phone number ({this.dialPrefix || \'with international dialing code\'})"> </div> <div class="w50p"> <label for="phone">Landline</label> <input name="phone" riot-value="{this.organisation.phone}" placeholder="Phone number ({this.dialPrefix || \'with international dialing code\'})"> </div> </div> <label for="email">Email</label> <input name="email" riot-value="{this.organisation.email}" placeholder="Email" onblur="{this.checkDuplicatesEmail}" type="{\'email\'}"> </div> </div> <duplicates href="{this.duplicatesEmailUri}"></duplicates> </fieldset> <fieldset> <div> <div> <details> <summary class="disease"> <legend class="disease">Diseases <small if="{this.diseasesCount}">({this.diseasesCount})</small></legend> Expand </summary> <div> <div class="row disease-list" each="{disease, index in this.organisation.diseases}" id="disease_{index}"> <select-autocomplete url="/disease/complete?limit=10&q=" class="row" name="diseases[]" riot-value="{disease.pk}" label="{disease.label}" placeholder="Name of the disease"></select-autocomplete> <button class="button disease" type="button" onclick="{this.removeDisease}" riot-value="{index}"> <icon figure="delete" embedded></icon> Delete </button> </div> <button class="button disease" type="button" onclick="{this.addDisease}"> <icon figure="add" embedded></icon> Add disease </button> </div> </details> </div> </div> </fieldset> <fieldset class="keywords"> <div> <div> <details> <summary class="keyword"> <legend class="keyword">Keywords <small if="{this.organisation.keywords.length}">({this.organisation.keywords.length})</small></legend> Expand </summary> <div each="{category in this.keywords}"> <h5>{category.kind}</h5> <div class="cols3"> <span each="{kw in category.values}"> <input type="checkbox" id="keywords_{kw.pk}" name="keywords[]" riot-value="{kw.pk}" checked="{this.organisationKeywordsPks.includes(kw.pk)}" class="keyword"> <label for="keywords_{kw.pk}"> <span>{kw.label}</span> </label> </span> </div> </div> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary> <legend>Memberships <small if="{this.membershipsCount}">({this.membershipsCount})</small></legend> Expand </summary> <div> <article class="memberships" id="membership_{index}" subform="membership[]" each="{organisationMembership, index in this.organisation.membership}"> <input type="hidden" riot-value="{JSON.stringify(organisationMembership.fees)}" name="fees"> <div class="row"> <div class="w63p"> <label for="status">Status</label> <select class="row" name="status" id="status"> <option value=""> --- </option> <option each="{status in this.membershipStatuses}" riot-value="{status.key}" selected="{organisationMembership.status == status.key}">{status.value}</option> </select> </div> <div> <label for="startdate">Start date</label> <input name="start_date" riot-value="{organisationMembership.start_date}" placeholder="From" type="{\'date\'}"> </div> <div> <label for="startdate">End date</label> <input name="end_date" riot-value="{organisationMembership.end_date}" placeholder="Until" type="{\'date\'}"> </div> </div> <div class="row"> <div class="w63p"> <label for="organisation">Organisation</label> <select-autocomplete url="/contact/complete?resource=organisation&limit=10&q=" name="organisation" riot-value="{organisationMembership.organisation &&organisationMembership.organisation.pk}" label="{organisationMembership.organisation.label}" placeholder="Name of the organisation"></select-autocomplete> </div> <div class="flex align-self-end"> <button type="button" class="button" onclick="{this.removeMembership}" riot-value="{index}"> <icon figure="delete" embedded></icon> Delete </button> </div> </div> </article> <button class="button" type="button" onclick="{this.addMembership}">Add a membership</button> </div> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary class="group"> <legend class="group">Groups <small if="{this.groupsCount}">({this.groupsCount})</small></legend> Expand </summary> <div each="{role in this.organisation.roles}" if="{Number(role.person.pk)}"> <h5>{role.person.label}</h5> <div class="groups row" subform="groups[]" each="{organisationGroup, index in role.groups}"> <input type="hidden" name="person" riot-value="{role.person.pk}"> <select name="group" required> <option value=""> - Group name - </option> <optgroup each="{category in this.groups}" label="{category.kind}"> <option each="{group in category.values}" riot-value="{group.pk}" selected="{organisationGroup.group.pk == group.pk}">{group.label}</option> </optgroup> </select> as <select name="role"> <option value=""> - Role - </option> <option each="{role in this.groupRoles}" riot-value="{role}" selected="{organisationGroup.role == role}">{role}</option> </select> <input type="checkbox" name="is_active" value="1" id="group_{organisationGroup.group.pk}_{index}" checked="{!organisationGroup.group.pk || organisationGroup.is_active}" class="group group_person_{role.person.pk}_active"> <label class="row" for="group_{organisationGroup.group.pk}_{index}">Still active</label> <button type="button" class="button group" onclick="{this.removeGroup}" person="{role.person.pk}" riot-value="{index}"> <icon figure="delete" embedded></icon> delete </button> </div> <button type="button" class="button group" onclick="{this.addGroup}" person="{role.person.pk}"> <icon figure="add" embedded></icon> Add a group </button> </div> <p if="{!this.organisation.pk}">You must first save this organisation before assigning groups.</p> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary> <legend>Organisation Contacts <small if="{this.rolesCount}">({this.rolesCount})</small></legend> Expand </summary> <div> <article hidden="{archivedPersonsPk.includes(role.person.pk)}" class="organisation" subform="roles[]" each="{role, index in this.organisation.roles}"> <div class="row"> <div class="w50p"> <label for="person">Name & Surname</label> <select-autocomplete url="/contact/complete?resource=person&limit=10&q=" name="person" riot-value="{role.person && role.person.pk}" label="{role.person.label}" placeholder="Name of the Person"></select-autocomplete> </div> <div class="w50p"> <label for="name">Role</label> <input class="row" name="name" placeholder="Role of this person" riot-value="{role.name}"> </div> </div> <div class="row"> <div class="w50p"> <label for="email">Email</label> <input id="role_email_{index}" class="row" name="email" placeholder="Specific email with this person" riot-value="{role.email}" type="{\'email\'}"> </div> <div class="w50p"> <label for="email">Specific Phone</label> <input class="row" name="phone" type="phone" placeholder="Specific phone with this person" riot-value="{role.phone}"> </div> <div class="flex w50p align-self-end"> <div class="primary-contact"> <input type="checkbox" id="is_primary_{index}" name="is_primary" value="1" checked="{role.is_primary || \'\'}"> <label for="is_primary_{index}">Primary contact</label> </div> <div class="ml-auto"> <button type="button" class="button" onclick="{this.removeRole}" riot-value="{index}" person-pk="{role.person.pk}"> <icon figure="delete" embedded></icon> Delete </button> </div> <input hidden type="checkbox" value="1" id="is_archived_{index}" class="archived-checkbox" name="is_archived" index="{index}" checked="{role.is_archived || \'\'}"> <input hidden type="checkbox" value="1" id="is_default_{index}" name="is_default" index="{index}" checked="{role.is_default || \'\'}"> </div> </div> </article> <button type="button" class="button" type="button" onclick="{this.addRole}"> <icon figure="add" embedded></icon> Link to a contact </button> </div> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary> <legend>Patient Organisation Details</legend> Expand </summary> <div> <label for="adherents">Number of Members</label> <input id="adherents" name="adherents" riot-value="{this.organisation.adherents}" type="{\'number\'}"> <label for="budget">Budget</label> <input id="budget" name="budget" riot-value="{this.organisation.budget}" type="{\'number\'}"> <label for="creation_year">Year of Creation</label> <input id="creation_year" name="creation_year" riot-value="{this.organisation.creation_year}" min="1900" max="{(new Date().getFullYear())}" type="{\'number\'}"> <label for="board">number of patient representatives / number of board directors</label> <input id="board" type="text" name="board" riot-value="{this.organisation.board}"> <label for="paid_staff">Number of paid staff</label> <input id="paid_staff" name="paid_staff" riot-value="{this.organisation.paid_staff}" type="{\'number\'}"> <label for="pharma_funding">Percentage of pharma funding</label> <input id="pharma_funding" name="pharma_funding" riot-value="{this.organisation.pharma_funding}" min="0" max="100" type="{\'number\'}"> </div> </details> </div> </div> </fieldset> <contact-attachment-edit contact="{this.organisation}"></contact-attachment-edit> <fieldset> <div> <div> <details open> <summary> <legend>Comment <small if="{this.organisation.comment}">(1)</small></legend> Expand </summary> <div> <label for="comment">Comment</label> <textarea name="comment" placeholder="Extra information about this organisation">{this.organisation.comment}</textarea> </div> </details> </div> </div> </fieldset> <section class="toolbar"> <div class="secondary"> <button type="button" if="{this.user.can(\'delete\')}" class="plain" onclick="{this.delete}"><icon figure="delete-small" embedded></icon>Delete</button> <button class="button" type="button" onclick="{() => history.back()}"><icon figure="close-small" embedded></icon>Cancel</button> <button class="button"><icon figure="save-small" embedded></icon>Save</button> </div> </section> </form> <link rel="stylesheet" href="/src/css/details.css">', 'organisation-edit select-autocomplete + select-autocomplete,[data-is="organisation-edit"] select-autocomplete + select-autocomplete{ margin-top: 0.8rem !important; } organisation-edit [type="checkbox"] + label,[data-is="organisation-edit"] [type="checkbox"] + label{ margin-bottom: 0.8rem; } organisation-edit select,[data-is="organisation-edit"] select{ width: 100%; } organisation-edit .keywords span,[data-is="organisation-edit"] .keywords span{ display: inline-block; } organisation-edit option.main,[data-is="organisation-edit"] option.main{ font-weight: bold; }', 'class="details"', function(opts) {
    this.user = window.user
    this.keywords = classifyByKind(config.KEYWORDS.filter(k => k.for_organisation))
    this.kinds = config.ORGANISATION_KIND.map(k => {
      k.is_main = String(k.key).endsWith('0')
      return k
    })
    this.groups = classifyByKind(config.GROUPS)
    this.groupRoles = config.ORGANISATION_TO_GROUP_ROLES
    this.membershipStatuses = config.MEMBERSHIP_STATUSES
    this.organisation = {}
    this.dbOrg = "{}"
    this.duplicatesNameUri = ''
    this.duplicatesEmailUri = ''
    this.duplicatesUrlUri = ''
    this.membershipsCount = 0
    this.rolesCount = 0
    this.diseasesCount = 0
    this.archivedPersonsPk = []

    this.on('*', () => {
      this.organisation = this.opts.organisation
      this.diseasesCount = document.getElementsByClassName("disease-list").length
      this.groupsCount = document.getElementsByClassName("groups").length
      this.membershipsCount = document.getElementsByClassName("memberships").length
      this.dbOrg = JSON.stringify(this.organisation)
      this.organisationKeywordsPks = this.organisation.keywords.map(k => k.pk)
      var archivedCheckboxes = document.getElementsByClassName("archived-checkbox")
      this.rolesCount = Array.prototype.reduce.call(
        archivedCheckboxes,
        (rolesCount, archivedCheckbox) => archivedCheckbox.checked ? rolesCount : rolesCount + 1,
        0
      )
    })

    this.on('mount', () => {
      const country = getCountry(this.organisation.country)
      this.dialPrefix = country && `+${country.prefix}`
      this.update()
    })

    this.changeCountry = (event) => {
      const country = getCountry(event.currentTarget.value)
      this.dialPrefix = country && `+${country.prefix}`
      this.update()
    }

    this.checkDuplicatesName = (event) => {
      if(this.organisation.pk) return
      this.duplicatesNameUri = `/complete?q=${event.target.value.trim()}&limit=3`
    }

    this.checkDuplicatesEmail = (event) => {
      if(this.organisation.pk) return
      this.duplicatesEmailUri = `/complete?q=${event.target.value.trim()}&limit=3`
    }

    this.checkDuplicatesUrl = (event) => {
      if(this.organisation.pk) return
      this.duplicatesUrlUri = `/complete?q=${event.target.value.trim()}&limit=3`
    }

    this.addDisease = (event) => {

      this.organisation.diseases.push({pk: ' ', label: ''})
      this.update()
    }

    this.removeDisease = (event) => {
      const disease = document.getElementById(`disease_${event.currentTarget.value}`)
      if (disease) {
        disease.remove()
      }
      this.update()
    }

    this.addGroup = (event) => {
      const personPk = event.currentTarget.getAttribute('person')
      const role = this.organisation.roles.find(r => r.person.pk == personPk)
      if(!role.groups) role.groups = []
      role.groups.push({})
    }

    this.removeGroup = (event) => {
      const button = event.currentTarget
      const role = this.organisation.roles.find(r => r.person.pk == button.getAttribute('person'))
      role.groups.splice(button.value, 1)
    }

    this.addMembership = () => {
      this.organisation.membership.push({organisation: {pk: ' ', label: ''}})
      this.update()
    }

    this.removeMembership = (event) => {
      const membership= document.getElementById(`membership_${event.currentTarget.value}`)
      if (membership) {
        membership.remove()
      }
      this.update()
    }

    this.save = async (event) => {
      event.preventDefault()
      const readonlyFields = ["pk", "attachment", "uri", "label", "resource", "groups", "created_at", "modified_at", "created_by", "modified_by", "filemaker_pk"]

      const dbOrgObj = JSON.parse(this.dbOrg)
      const comparableOrg = {
        ...dbOrgObj,
        events: dbOrgObj.events || [],
        cell_phone: String(dbOrgObj.cell_phone || ''),
        phone: String(dbOrgObj.phone || ''),
        diseases: (dbOrgObj.diseases || []).map(o => Number(o.pk)),
        keywords: sort((dbOrgObj.keywords || []).map(o => Number(o.pk))),
        membership: (dbOrgObj.membership || []).map(o => new Membership(Object.assign(o, { organisation: o.organisation.pk }))),
        roles: (dbOrgObj.roles || []).map(o => new Role(Object.assign(
          o,
          {
            person: o.person.pk,
            organisation: o.organisation && o.organisation.pk,
          }
        )))
      }

      const formData = serializeForm(event.target, true)
      const newOrg = {
        ...formData,
        cell_phone: String(formData.cell_phone || ''),
        phone: String(formData.phone || ''),
        board: String(formData.board || ''),
        keywords: sort(formData.keywords || []).filter(k=>k),
        diseases: (formData.diseases || []),
        membership: (formData.membership || []).map(o => new Membership(o)),
        roles: (formData.roles || []).map(role => new Role(Object.assign(
          comparableOrg.roles.find(r => r.person == role.person) || {},
          {
            person: role.person,
            organisation: this.organisation,
            groups: (formData.groups || []).filter(g => g.person == role.person),
          },
          role,
        )))
      }

      const data = diffing(comparableOrg, newOrg, readonlyFields)
      const dataDiff = !!Object.keys(data).length

      const response = dataDiff ? (this.organisation.pk
        ? apiResponse(await api(`/organisation/${this.organisation.pk}`, { method: 'patch', data }))
        : apiResponse(await api(`/organisation`, { method: 'post', data }))) : null
      if (!dataDiff | response?.ok) {
        if(response?.ok) {
          const organisation = response.data

          await document.querySelector('contact-attachment-edit')._tag.save(organisation)
          page(`/organisation/${organisation.pk}${location.search || ''}`)
        }
        notify(`Contact was saved successfully`, 'success')
      }
    }

    this.addRole = () => {
      this.organisation.roles.push({ person: { pk: ' ' }, groups: [] })
    }

    this.removeRole = (event) => {
      if (!window.confirm('Are you sure you want to remove this role ?'))
        return
      const personPk = event.currentTarget.getAttribute('person-pk')
      if (personPk == ' ') {
        this.organisation.roles.splice(event.target.value, 1)
      } else {

        document.getElementById(`is_archived_${event.currentTarget.value}`).checked = true
        document.getElementById(`is_default_${event.currentTarget.value}`).checked = false
        document.getElementById(`role_email_${event.currentTarget.value}`).value = null
        this.archivedPersonsPk.push(personPk)

        groups_active = document.getElementsByClassName(`group_person_${personPk}_active`)
        for (const group_active of groups_active) {
          group_active.checked = false
        }
      }
      this.update()
    }

    this.delete = async (event) => {
      if(!confirm('You are about to permanently delete this organisation. Are your sure?')) return
      const response = apiResponse(await api(`/organisation/${this.organisation.pk}`, { method: 'delete' }))
      if (response.ok) {
        notify(`The organisation "${this.organisation.name}" was deleted.`, 'success')
        page(`/${location.search}`)
      }
      else {
        notify(`Could not delete organisation: ${response.data.error}`, 'error')
      }

    }
});

riot.tag2('organisation-fees-edit', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-fees"></icon> </div> </div> <div> <h1>Fees for {this.organisation.label}</h1> </div> </header> <form onsubmit="{this.save}"> <fieldset subform="membership[]" each="{membership, index in this.memberships}" show="{membership.organisation.name.toLowerCase() == ⁗eurordis⁗}"> <div> <div> <h2>{membership.organisation.label} <small>(since {trans(new Date(membership.start_date))})</small></h2> <button class="button" type="button" onclick="{this.addFee}" index="{index}"><icon figure="add" embedded></icon> Add a fee</button> <article subform="fees[]" each="{fee, f in membership.fees}" class="row"> <div class="year"> <label for="year">Year</label> <input class="row" name="year" id="year" riot-value="{fee.year}" placeholder="Year" required min="1997" max="{(new Date()).getFullYear() + 2}" type="{\'number\'}"> </div> <div> <label for="amout">Amount (€)</label> <input class="row" name="amount" id="amount" riot-value="{fee.amount}" required disabled="{fee.exempted}" type="{\'number\'}"> </div> <div class="date"> <label for="payment_date">Date</label> <input class="row" name="payment_date" riot-value="{fee.payment_date}" required disabled="{fee.exempted && (fee.exempted).toISOString()}" type="{\'date\'}"> </div> <div> <div> <input type="checkbox" value="1" id="fee-{membership.organisation.label}-{f}" checked="{fee.exempted}" name="exempted" onchange="{this.toggleExempted}" membership-index="{index}" fee-index="{f}"> <label class="row" for="fee-{membership.organisation.label}-{f}"> Exempted for this year </label> </div> <div> <input type="checkbox" value="1" id="receipt-{membership.organisation.label}-{f}" checked="{fee.receipt_sent}" name="receipt_sent" onchange="{this.toggleReceiptSent}" membership-index="{index}" fee-index="{f}"> <label class="row" for="receipt-{membership.organisation.label}-{f}"> Receipt was sent </label> </div> </div> <div class="row" if="{!fee.receipt_sent}"> <select id="recipient" onchange="{this.updateRecipient}" membership-index="{index}" fee-index="{f}"> <option selected="{fee.recipient === recipient.pk}" each="{recipient in this.recipients}" name="_recipient" riot-value="{recipient.pk}">{recipient.label}</option> </select> <button type="button" onclick="{this.sendReceipt}">Send a receipt</button> </div> <div> <button class="button" type="button" onclick="{this.removeFee}" membership-index="{index}" fee-index="{f}"> <icon figure="delete" embedded></icon> Delete this fee </button> </div> </article> </div> </div> </fieldset> <section class="toolbar"> <a class="button" href="{getUrl(\'/organisation/\'+this.organisation.pk)}"><icon figure="close-small" embedded></icon> Cancel</a> <button class="button"><icon figure="save-small" embedded></icon> Save</button> </section> </form> <link rel="stylesheet" href="/src/css/details.css">', 'organisation-fees-edit fieldset,[data-is="organisation-fees-edit"] fieldset{ margin-bottom: 1.2rem; } organisation-fees-edit fieldset > div,[data-is="organisation-fees-edit"] fieldset > div{ margin-bottom: 1.2rem !important; } organisation-fees-edit article,[data-is="organisation-fees-edit"] article{ display: flex; flex-wrap: wrap; justify-content: space-between; align-items: flex-end; margin-top: 0.8rem; } organisation-fees-edit article:after,[data-is="organisation-fees-edit"] article:after{ display: none; } organisation-fees-edit article > div:nth-last-of-type(2),[data-is="organisation-fees-edit"] article > div:nth-last-of-type(2){ margin-right: 0; } organisation-fees-edit article > div:last-of-type,[data-is="organisation-fees-edit"] article > div:last-of-type{ flex-basis: 100% } organisation-fees-edit article .button,[data-is="organisation-fees-edit"] article .button{ margin-top: 0.4rem; } organisation-fees-edit label,[data-is="organisation-fees-edit"] label{ margin-top: 0; } organisation-fees-edit .year,[data-is="organisation-fees-edit"] .year{ max-width: 5.6rem; } organisation-fees-edit .date,[data-is="organisation-fees-edit"] .date{ max-width: 9rem; } organisation-fees-edit h2,[data-is="organisation-fees-edit"] h2{ font-weight: bold; margin-bottom: 0.4rem; } organisation-fees-edit h2 small,[data-is="organisation-fees-edit"] h2 small{ font-family: "IBMPlexSans", sans-serif; font-size: 0.7rem; font-weight: 300; }', 'class="details"', function(opts) {
    function strToTimestamp(str) {
      return (new Date(str)).getTime()
    }

    this.on('*', () => {
      this.organisation = this.opts.organisation
      this.memberships = this.organisation.membership.sort((a, b) => {
        return strToTimestamp(b.start_date) - strToTimestamp(a.start_date)
      })
      this.recipients = this.organisation.roles.filter(r => r.is_primary).map(r => ({pk: r.person.pk, label: r.person.label}))
    })

    this.on('mount', () => {
      document.querySelector('main').scrollTo(0, 0)
    })

    this.addFee = (event) => {
      const membership = this.memberships[event.currentTarget.getAttribute('index')]
      if(!membership.fees) membership.fees = []
      const date = new Date()
      membership.fees.unshift({year: date.getFullYear(), payment_date: date.toISOString().slice(0, 10)})
      this.update()
      this.root.querySelector('[name=amount]').focus()
    }

    this.removeFee = (event) => {
      const membership = this.memberships[event.currentTarget.getAttribute('membership-index')]
      membership.fees.splice(event.currentTarget.getAttribute('fee-index'), 1)
    }

    this.toggleExempted = (event) => {
      const checkbox = event.currentTarget
      const fee = this.memberships[checkbox.getAttribute('membership-index')].fees[checkbox.getAttribute('fee-index')]
      fee.exempted = event.target.checked
      this.update()
    }

    this.toggleReceiptSent = (event) => {
      const checkbox = event.currentTarget
      const fee = this.memberships[checkbox.getAttribute('membership-index')].fees[checkbox.getAttribute('fee-index')]
      fee.receipt_sent = event.target.checked
      this.update()
    }

    this.updateRecipient = (event) => {
      const membership = this.memberships[event.currentTarget.getAttribute('membership-index')]
      membership.fees[event.currentTarget.getAttribute('fee-index')].recipient = event.target.value
    }

    this.sendReceipt = async (event) => {
      const container = event.target.closest('[subform="fees[]"]')
      const year = Number(container.querySelector('[name=year]').value)
      const response = apiResponse(await api('/send-receipt', {
        method: 'POST',
        data: {
          organisation: this.organisation.pk,
          person: container.querySelector('#recipient').value,
          year,
        }
      }))
      if(!response.ok) return
      notify(`The receipt for ${year} was sent by email`, 'success')
    }

    this.save = async (event) => {
      event.preventDefault()
      const data = serializeForm(event.target)
      const memberships = this.memberships.map((membership, i) => {
        membership.fees = data.membership[i] && data.membership[i].fees
        return membership
      })
      const response = apiResponse(await api(`/organisation/${ this.organisation.pk }`, {
        method: 'PATCH',
        data: { membership: memberships }
      }))
      if(!response.ok) return
      notify(`The fees were saved successfully`, 'success')
      page(getUrl(`/organisation/${this.organisation.pk}`))
    }
});

riot.tag2('results', '<header> <div class="{row: true, disabled: !this.opts.filters.length}"> <div> <h3 if="{this.count}"> {this.count} result<span if="{this.count> 1}">s</span> <span if="{this.total> this.count}"> out of {this.total}</span> </h3> <h3 if="{!this.count}">0 result</h3> <a href="{location.pathname}" class="button" title="Clear all filters"> <icon figure="delete" embedded></icon> Delete all filters</a> </div> <div class="secondary"> <div class="type-filter"> <button onclick="{this.filter}" value="" class="{button: true, icon: true, active: this.isSelected(\'resource\',             null)}" title="Filter contacts and organisations"> <icon figure="peopleandorganisation" embedded></icon> {this.isSelected(\'resource\',             null) && this.opts.total} </button> <button onclick="{this.filter}" value="person" class="{button: true, icon: true, active:             this.isSelected(\'resource\', \'person\' )}" title="Filter only contacts"> <icon figure="person" embedded></icon> {this.opts.personsTotal} </button> <button onclick="{this.filter}" value="organisation" class="{button: true, icon: true, active:             this.isSelected(\'resource\', \'organisation\' )}" title="Filter only organisations"> <icon figure="organisation" embedded></icon> {this.opts.organisationsTotal} </button> </div> </div> </div> <div class="{\'add-line\' : true}"> Add: <button class="{plain: true, disabled: !this.opts.personsTotal}" title="Export all listed contacts" onclick="{this.addListedPersons}"> <icon figure="person"></icon> Contacts </button> <button class="{plain: true, disabled: !this.opts.organisationsTotal}" title="Export all listed organisations" onclick="{this.addListedOrganisations}"> <icon figure="organisation"></icon> Organisations </button> <button class="{plain: true, disabled: !this.opts.organisationsTotal}" title="Export primary contacts from listed organisations" onclick="{this.addAllPrimaries}"> <icon figure="primary"></icon> Primary contacts </button> </div> </header> <nav> <div each="{result in this.opts.results}"> <a riot-value="{result.pk}" type="{result.resource}" class="{active: (this.opts.selectedPk &&         this.opts.selectedPk==result.pk)}" onclick="{this.selectItem}" href="{getUrl(\'/\'+result.resource+\'/\'+result.pk)}" title="{result.label}"> <icon figure="{result.resource}"></icon> <country-flag country="{result.country}"></country-flag> <span>{result.label}</span> </a> <add-export if="{result.role}" person-pk="{result.resource==\'person\' ? result.pk : result.role.person.pk}" organisation-pk="{result.resource==\'organisation\' ? result.pk : result.role.organisation.pk}"></add-export> <add-export if="{!result.role}" person-pk="{result.resource==\'person\' && result.pk}" organisation-pk="{result.resource==\'organisation\' && result.pk}"></add-export> </div> <a class="show-all" href="{replaceQueryParams({ limit: \'false\' })}" if="{this.total> this.count}">Show All (slow)</a> </nav>', 'results .row,[data-is="results"] .row{ width: 100%; margin: 0 0 .3rem; justify-content: space-between; align-items: flex-start; } results header,[data-is="results"] header{ display: flex; justify-content: space-between; align-items: center; flex-wrap: wrap; position: sticky; top: 0; right: 0; min-height: 3rem; margin: 0 0 1.8rem; padding: 2rem 1.6rem; background-color: var(--secondary-background-color); z-index: 3; } results header:after,[data-is="results"] header:after{ content: \'\'; position: absolute; right: 0; bottom: 0; left: 0; width: calc(100% - 3.2rem); height: 1px; margin: auto; background-color: var(--border-color); } results header .type-filter .icon,[data-is="results"] header .type-filter .icon{ display: inline-flex; justify-content: center; align-items: center; } results button.icon icon img,[data-is="results"] button.icon icon img,results button.icon icon svg,[data-is="results"] button.icon icon svg{ display: block; width: 1rem !important; height: auto; margin-top: -.1rem; margin-left: .2rem; } results header .secondary,[data-is="results"] header .secondary{ display: flex; align-items: center; } results header .secondary .button:not(:first-child),[data-is="results"] header .secondary .button:not(:first-child){ margin-left: 0.4rem; } results h3,[data-is="results"] h3{ font-size: 1.2rem; } results h3+.button,[data-is="results"] h3+.button{ padding: 0; border: none; border-radius: 0; } results h3+.button:hover,[data-is="results"] h3+.button:hover{ color: inherit; background: none; } results h3+.button:hover svg g,[data-is="results"] h3+.button:hover svg g,results h3+.button:hover svg path,[data-is="results"] h3+.button:hover svg path{ stroke: var(--primary-color); } results .add-line,[data-is="results"] .add-line{ max-width: 100%; display: flex; align-items: center; margin-top: 0.8rem; font-size: 0.7rem; color: var(--primary-color); flex-wrap: inherit; } results .add-line+.add-line,[data-is="results"] .add-line+.add-line{ margin-top: 0; } results .add-line+.add-line :first-child,[data-is="results"] .add-line+.add-line :first-child{ padding-left: 0; } results .add-line button,[data-is="results"] .add-line button{ padding: 0 0.4rem; border-radius: 0; } results .add-line button:not(:first-of-type),[data-is="results"] .add-line button:not(:first-of-type){ border-left: 1px solid var(--border-color); } results img,[data-is="results"] img{ width: 1.2rem; } results nav,[data-is="results"] nav{ border-right: 1px solid var(--border-color); } results nav div,[data-is="results"] nav div{ position: relative; } results nav [figure=person],[data-is="results"] nav [figure=person],results nav [figure=organisation],[data-is="results"] nav [figure=organisation]{ height: 1.2rem; margin-right: .3rem; } results nav icon img,[data-is="results"] nav icon img{ vertical-align: bottom; } results nav span,[data-is="results"] nav span{ display: inline-block; max-width: 75%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; } results nav a,[data-is="results"] nav a{ display: flex; align-items: center; padding: .3rem 1.6rem .3rem; text-decoration: none; color: var(--text-color); border-top: 1px solid transparent; border-bottom: 1px solid transparent; } results nav a:hover,[data-is="results"] nav a:hover,results nav a.active,[data-is="results"] nav a.active{ text-decoration: none; background-color: var(--primary-color-light); border-color: rgba(0, 98, 183, 0.07) } results nav country-flag,[data-is="results"] nav country-flag{ margin-right: 0.4rem; } results nav add-export,[data-is="results"] nav add-export{ position: absolute; top: .3rem; right: 1.6rem; } results .type-filter,[data-is="results"] .type-filter{ display: flex; align-items: center; color: var(--primary-color); font-weight: 500; } results .disabled,[data-is="results"] .disabled{ position: relative; opacity: 0.3; } results .disabled::before,[data-is="results"] .disabled::before{ content: \'\'; position: absolute; top: 0; right: 0; bottom: 0; left: 0; background-color: transparent; } results .show-all,[data-is="results"] .show-all{ display: block; text-align: center; }', '', function(opts) {

    this.query = ''
    this.results = []
    this.total = 0
    this.count = 0

    this.on('*', () => {
      this.query = this.opts.query
      this.results = this.opts.results
      this.total = this.opts.total
      this.count = this.results && this.results.length
    })

    this.getAllResults = async () => {
      showLoader()
      if (this.total <= this.count) return this.results

      const filters = this.opts.filters
      filters.push({
        name: 'limit',
        value: 0
      })
      const response = await searchResults(filters)
      return response.data
    }

    this.addAllPrimaries = async () => {
      const results = await this.getAllResults()

      let resources = []
      results.forEach(result => {
        if (result.resource !== 'organisation') return
        if (result.roles && result.roles.length) resources = resources.concat(result.roles.filter(r => r
          .is_primary))
      })
      exportList.addMany(resources)

      updatePage()
    }

    this.addListedPersons = async () => {
      const results = await this.getAllResults()
      let resources = results.filter(r => r.resource === 'person').map(r => {
        const result = {
          person: r.pk
        }
        if (r.role && r.role.organisation) result.organisation = r.role.organisation.pk
        return result
      })
      exportList.addMany(resources)
      updatePage()
    }

    this.addListedOrganisations = async () => {
      const results = await this.getAllResults()
      const resources = results.filter(r => r.resource === 'organisation').map(r => ({
        organisation: r.pk
      }))
      exportList.addMany(resources)

      updatePage()
    }

    this.filter = (event) => {
      const resourceType = event.currentTarget.getAttribute('value')
      page(replaceQueryParams({
        resource: resourceType
      }))
    }

    this.isSelected = function(group, value) {
      const groupFilter = this.opts.filters.find(f => f.name == group)
      if(!groupFilter) return !value
      return groupFilter.value == value
    }.bind(this)

});

riot.tag2('role-card', '<article class="{default: this.role.is_default}"> <icon if="{this.role.is_primary}" figure="primary" title="Primary contact for this organisation"></icon> <icon if="{!this.role.is_primary}" figure="organisation"></icon> <h3> <a href="/organisation/{this.role.organisation.pk}{location.search}">{this.role.organisation.name}</a> <span if="{this.role.name}">| {this.role.name}</span> </h3> <ul if="{(this.role && (this.role.email || this.role.phone)) || (this.organisation && this.organisation.email)}"> <li if="{this.role.email}"> <icon figure="email"></icon> <a target="_blank" href="mailto:{this.role.name}<{this.role.email}>">{this.role.email}</a> </li> <li if="{this.role.phone}"> <icon figure="phone"></icon> <span>{this.role.phone}</span> </li> </ul> <icon class="voting-contact-icon" if="{this.role.is_voting}" figure="votingcontact" label="Voting contact for this organisation"></icon> </article>', 'role-card,[data-is="role-card"]{ display: block; } role-card header,[data-is="role-card"] header{ padding: 0 .3rem; } role-card article,[data-is="role-card"] article{ width: 100%; } role-card article.default,[data-is="role-card"] article.default{ background: transparent url(\'/src/images/icons/default.svg\') no-repeat 0 6px; padding-left: 1.5rem; margin-left: -1.5rem; } role-card a,[data-is="role-card"] a{ text-decoration: none; color: var(--primary-color); vertical-align: middle; border-bottom: none; } role-card icon,[data-is="role-card"] icon{ display: inline-block; height: 1.3rem; } role-card h1,[data-is="role-card"] h1,role-card h2,[data-is="role-card"] h2{ font-family: inherit; font-size: .8rem; font-weight: normal; } role-card h1,[data-is="role-card"] h1{ display: inline-block; margin: 0 auto; font-family: inherit; vertical-align: middle; } role-card h2,[data-is="role-card"] h2{ margin-top: 0.4rem; text-transform: none; } role-card main,[data-is="role-card"] main{ padding-top: 1rem; padding-bottom: .7rem; } role-card ul,[data-is="role-card"] ul,role-card li,[data-is="role-card"] li{ margin: 0!important; padding: 0; } role-card [figure=email] + a,[data-is="role-card"] [figure=email] + a{ color: var(--text-color); margin-top: -.2rem!important; } role-card li + li,[data-is="role-card"] li + li{ margin-left: 2rem !important; } role-card ul,[data-is="role-card"] ul,role-card li,[data-is="role-card"] li,role-card li icon,[data-is="role-card"] li icon,role-card .voting-contact-icon,[data-is="role-card"] .voting-contact-icon{ display: flex; align-items: center; justify-content: start; } role-card .voting-contact-icon span,[data-is="role-card"] .voting-contact-icon span{ margin-left: .3rem; } role-card span,[data-is="role-card"] span{ vertical-align: middle; } role-card h3,[data-is="role-card"] h3{ font-weight: 500!important; display: inline-block; font-size: .8rem; color: inherit; margin: 0 0 .3rem .2rem; }', '', function(opts) {
    this.role = {}

    this.on('mount', async () => {
      this.role = this.opts.role
      const response = apiResponse(await api(this.role.organisation.uri))
      this.organisation = response.data
      this.update()
    })
});

riot.tag2('search-box', '<form ref="form" onsubmit="return false" class="row collapsed" onmouseenter="{() => this.showSuggestions = true}" onmouseleave="{() => this.showSuggestions = false}"> <div class="search-icon"> <input type="search" autofocus oninput="{this.updateQuery}" name="q" riot-value="{this.opts.contact || this.opts.q}" placeholder="Search and/or filter" autocomplete="off"> </div> <div id="suggestions" class="{displayed: this.showSuggestions}"> <div each="{suggestion in this.suggestions}" class="row {suggestion.resource}"> <virtual if="{[\'keyword\', \'grouping\', \'country\', \'group\', \'group_role\', \'kind\', \'event_kind\', \'event_role\', \'keyword_kind\', \'group_kind\'].includes(suggestion.resource)}"> <div><icon figure="{suggestion.resource}" title="{suggestion.resource}"></icon> <span title="{suggestion.description}">{suggestion.label}</span> </div> <div class="secondary"> <button type="button" onclick="{this.filter}" name="{suggestion.resource}" riot-value="{suggestion.pk}" label="{suggestion.label}">Filter</button> <a aria-disabled="true"><span>View</span></a> </div> </virtual> <virtual if="{[\'person\', \'organisation\'].includes(suggestion.resource)}"> <div><icon figure="{suggestion.resource}" title="{suggestion.resource}"></icon> <span>{suggestion.label}</span> </div> <div class="secondary"> <button type="button" disabled>Filter</button> <a href="/{suggestion.resource}/{suggestion.pk}">View</a> </div> </virtual> <virtual if="{suggestion.resource == \'event\'}"> <div><icon figure="{suggestion.resource}" title="Event"></icon> <span>{suggestion.label}</span> </div> <div class="secondary"> <button type="button" onclick="{this.filter}" name="{suggestion.resource}" riot-value="{suggestion.pk}" label="{suggestion.label}">Filter</button> <a href="/{suggestion.resource}/{suggestion.pk}">View</a> </div> </virtual> <virtual if="{suggestion.resource == \'disease\'}"> <div> <icon figure="disease" title="Disease"></icon> <disease-gauge depth="{suggestion.depth}" max-depth="{suggestion.max_depth}"></disease-gauge> <div> <span>{suggestion.label}</span> <br> <span class="orphanet" if="{suggestion.pk}">Orphanet: <a href="https://www.orpha.net/consor/cgi-bin/OC_Exp.php?Expert={suggestion.pk}" title="View on Orphanet" target="_blank">{suggestion.pk}</a></span> <a href="{getUrl(\'/disease/\'+suggestion.pk)}">Database card</a> </div> </div> <div class="secondary"> <button type="button" class="plain" disabled="{suggestion.depth == 0}" onclick="{this.filter}" name="disease" riot-value="<{suggestion.pk}" label="{suggestion.label}" title="Filter all items related to this disease and one of its parents"> <icon figure="ascendants"></icon> </button> <button type="button" class="plain" onclick="{this.filter}" name="disease" riot-value="{suggestion.pk}" label="{suggestion.label}" title="Filter all items related to this disease"> <icon figure="root"></icon> </button> <button type="button" class="plain" disabled="{suggestion.depth == suggestion.max_depth}" onclick="{this.filter}" name="disease" riot-value=">{suggestion.pk}" label="{suggestion.label}" title="Filter all items related to this disease and one of its children"> <icon figure="descendants"></icon> </button> </div> </virtual> </div> <div if="{this.q}"> <button type="button" onclick="{this.filter}" name="contact" riot-value="{this.q}" label="{this.q}" class="secondary"> Search "{this.q}" in all contacts </button> </div> </div> </form>', 'search-box,[data-is="search-box"]{ position: relative; margin-right: 1.6rem !important; } search-box .search-icon,[data-is="search-box"] .search-icon{ position: relative; } search-box .search-icon::before,[data-is="search-box"] .search-icon::before{ content: \'\'; position: absolute; top: 0.7rem; left: 1rem; display: block; width: 1.2rem; height: 1.2rem; background: url(/src/images/icons/search.svg) 0 0 no-repeat; background-size: 1.2rem; z-index: 3; } search-box form,[data-is="search-box"] form{ margin: 0 !important; } search-box [type=search],[data-is="search-box"] [type=search]{ width: 30rem; height: 2.45rem; padding-left: 2.4rem; border-color: var(--border-color); z-index: 2; border-radius: .2rem; font-weight: 500; } search-box [type=search]:focus + button,[data-is="search-box"] [type=search]:focus + button{ content: "Full search"; box-shadow: 0 0 .2rem var(--primary-color); } search-box [type=search]::placeholder,[data-is="search-box"] [type=search]::placeholder{ font-weight: normal; } search-box #suggestions,[data-is="search-box"] #suggestions{ position: absolute; top: 100%; right: 0; display: block; min-width: 100%; max-width: 60vw; padding: 0 1.6rem; width: max-content; background-color: #fff; border: 1px solid var(--border-color); box-shadow: 0 6px 6px 0 rgba(0,0,0,0.1); z-index: 1; } search-box #suggestions:not(.displayed),[data-is="search-box"] #suggestions:not(.displayed){ display: none; } search-box #suggestions > .row,[data-is="search-box"] #suggestions > .row{ flex-wrap: wrap; justify-content: space-between; margin: 0; padding: 0 0 1.2rem; } search-box #suggestions > .row:first-child,[data-is="search-box"] #suggestions > .row:first-child{ padding-top: 1.2rem; } search-box #suggestions > .row:nth-last-of-type(2),[data-is="search-box"] #suggestions > .row:nth-last-of-type(2){ padding-bottom: 0; } search-box #suggestions > :last-child,[data-is="search-box"] #suggestions > :last-child{ display: flex; justify-content: center; padding: 1.2rem 0; } search-box #suggestions > .row:after,[data-is="search-box"] #suggestions > .row:after{ content: ""; display: block; height: 1px; width: 3.6rem; margin-top: 1.2rem; background-color: var(--border-color); } search-box #suggestions > .disease:last-of-type,[data-is="search-box"] #suggestions > .disease:last-of-type{ width: 100%; } search-box #suggestions .row > div:first-of-type,[data-is="search-box"] #suggestions .row > div:first-of-type{ display: flex; align-items: center; width: calc(100% - 5.8rem); font-weight: 500; } search-box #suggestions :not(.disease) span,[data-is="search-box"] #suggestions :not(.disease) span,search-box #suggestions .disease > div > div,[data-is="search-box"] #suggestions .disease > div > div{ text-overflow: ellipsis; white-space: nowrap; overflow: hidden; } search-box .secondary button,[data-is="search-box"] .secondary button,search-box .secondary a,[data-is="search-box"] .secondary a{ width: auto; height: auto !important; margin-right: 0; padding: 0; font-size: 0.7rem; color: var(--primary-color); font-weight: 500; text-decoration: none; border: none; } search-box .secondary a,[data-is="search-box"] .secondary a{ margin-left: 0.8rem; padding-left: 0.8rem; border-left: 1px solid var(--border-color); } search-box .secondary [name=disease]:not(:first-of-type),[data-is="search-box"] .secondary [name=disease]:not(:first-of-type){ margin-left: 0.6rem; } search-box #suggestions .row > .secondary,[data-is="search-box"] #suggestions .row > .secondary{ display: flex; justify-content: flex-end; align-items: center; flex-basis: 4.4rem; } search-box #suggestions .disease .secondary button,[data-is="search-box"] #suggestions .disease .secondary button{ width: auto; } search-box #suggestions .disease .secondary button:hover,[data-is="search-box"] #suggestions .disease .secondary button:hover{ background-color: transparent; } search-box #suggestions .disease .secondary icon,[data-is="search-box"] #suggestions .disease .secondary icon{ width: auto; margin-right: 0; } search-box #suggestions icon,[data-is="search-box"] #suggestions icon{ width: 1.45rem; height: auto; margin-right: .5rem; } search-box #suggestions icon[figure=disease],[data-is="search-box"] #suggestions icon[figure=disease],search-box #suggestions icon[figure^=gauge],[data-is="search-box"] #suggestions icon[figure^=gauge]{ margin-right: 0; } search-box .disease,[data-is="search-box"] .disease,search-box .disease a,[data-is="search-box"] .disease a{ color: var(--disease); } search-box .disease .orphanet,[data-is="search-box"] .disease .orphanet{ font-weight: normal; color: #50596c; } search-box .disease span + a,[data-is="search-box"] .disease span + a{ margin-left: 0.4rem; padding-left: 0.4rem; font-weight: normal; border-left: 1px solid var(--border-color); } search-box .country,[data-is="search-box"] .country,search-box .country button,[data-is="search-box"] .country button{ color: var(--country); } search-box .country button,[data-is="search-box"] .country button{ border-color: var(--country); } search-box .group,[data-is="search-box"] .group,search-box .group button,[data-is="search-box"] .group button{ color: var(--group); } search-box .group button,[data-is="search-box"] .group button{ border-color: var(--group); } search-box [figure=organisation] img,[data-is="search-box"] [figure=organisation] img,search-box [figure=disease] img,[data-is="search-box"] [figure=disease] img,search-box [figure=person] img,[data-is="search-box"] [figure=person] img,search-box [figure=grouping] img,[data-is="search-box"] [figure=grouping] img{ width: 1.6rem; height: 1.6rem; } search-box [figure=country] img,[data-is="search-box"] [figure=country] img,search-box [figure^=gauge] img,[data-is="search-box"] [figure^=gauge] img{ width: 1.2rem; height: 1.2rem; } search-box [figure=ascendants] img,[data-is="search-box"] [figure=ascendants] img,search-box [figure=descendants] img,[data-is="search-box"] [figure=descendants] img{ width: 1.2rem !important; } search-box [figure=root] img,[data-is="search-box"] [figure=root] img{ width: .9rem !important; } search-box [figure=disease],[data-is="search-box"] [figure=disease],search-box disease-gauge,[data-is="search-box"] disease-gauge{ align-self: flex-start; }', '', function(opts) {
    this.q = this.opts.q || ''
    this.timeoutId = null

    this.on('mount', () => {
      this.updateBox()
    })

    this.updateQuery = (event) => {
      this.q = event.target.value
      if (this.timeoutId) clearTimeout(this.timeoutId)
      this.timeoutId = setTimeout(() => {
        this.updateBox()
        this.showSuggestions = true
      }, 400)
    }

    this.filter = (event) => {
      const button = event.currentTarget
      this.filters = this.opts.filters || []
      !this.filters.find(f => f.value === button.value && f.name === button.name) && this.filters.push({
        name: button.name,
        value: button.value,
        label: button.getAttribute('label') || button.value,
      })
      const obj = {}
      this.filters.forEach(f => {
        if(!(f.name in obj)) obj[f.name] = []
        obj[f.name].push(`${f.value}|${f.label}`)
      })
      obj.q = this.q
      obj.showFacets = true
      page(getUrl('/', obj))
    }

    this.updateBox = async () => {
      this.q = this.q.trim()
      this.suggestions = []
      if (this.q.length < 3) {
        this.showSuggestions = false
        return
      }

      const response = apiResponse(await api(`/complete?q=${this.q}&limit=10`))
      this.suggestions = response.data.data

      this.update()
    }
});

riot.tag2('select-autocomplete', '<form class="{error: this.hasError}" onmouseenter="{() => this.showSuggestions = true}" onmouseleave="{() => this.showSuggestions = false}" onmouseleave="{this.validate}"> <input type="search" riot-value="{this.q}" oninput="{this.autocomplete}" placeholder="{this.opts.placeholder}" onfocusout="{this.validate}"> <ul class="{displayed: this.showSuggestions}"> <li each="{item in this.items}" data-value="{item.pk}" onclick="{this.select}">{item.label}</li> </ul> </form>', 'select-autocomplete,[data-is="select-autocomplete"]{ display: block; position: relative; } select-autocomplete > div,[data-is="select-autocomplete"] > div{ width: 100% !important; } select-autocomplete form,[data-is="select-autocomplete"] form{ margin: 0; } select-autocomplete input[type=search],[data-is="select-autocomplete"] input[type=search]{ padding-right: 1.5rem; } select-autocomplete .clear,[data-is="select-autocomplete"] .clear{ position: absolute; top: 0; right: .2rem; bottom: 0; margin: auto; height: 1rem; width: 1.2rem; font-size: 1.3rem; } select-autocomplete ul,[data-is="select-autocomplete"] ul{ display: block; } select-autocomplete ul:not(.displayed),[data-is="select-autocomplete"] ul:not(.displayed){ display: none; } select-autocomplete ul,[data-is="select-autocomplete"] ul{ position: absolute; top: 100%; left: 0; min-width: 100%; width: max-content; width: -moz-max-content; width: -webkit-max-content; width: -ie-max-content; padding: 0; margin: 0; background-color: #fff; list-style: none; border: 1px solid #eee; z-index: 2; } select-autocomplete li,[data-is="select-autocomplete"] li{ min-width: 100%; padding: .3rem .5rem; background-color: #fff; border-bottom: 1px solid #eee; cursor: pointer; } select-autocomplete li:hover,[data-is="select-autocomplete"] li:hover{ background-color: #eee; } select-autocomplete .error input,[data-is="select-autocomplete"] .error input{ border-color: red; box-shadow: 0 0 3px red; }', '', function(opts) {
    this.value = null
    this.q = this.opts.label || ''
    this.items = []
    this.showSuggestions = false
    this.hasError = false

    this.autocomplete = async (event) => {
      this.q = event.target.value
      if(this.q.length < 3) return
      const searchQuery = this.opts.url + (this.q ? this.q : '')
      const response = await api(`${searchQuery}`)
      this.items = response.data.data
      this.showSuggestions = true
      this.update()
    }

    this.validate = (event) => {
      const text = event.currentTarget.value
      this.hasError = false
      if (!text) return this.setValue('')
      else if (this.items.length) {
        this.q = this.items[0].name
        return this.setValue(this.items[0].pk)
      }
      else if (text && text !== this.opts.label) {
        this.hasError = true
        this.q = this.opts.label
        return
      }
    }

    this.root.checkValidity = () => this.hasError

    this.clear = () => {
      this.q = ''
      this.root.value = this.root.setAttribute('value', '')
      this.items = []
      this.update()
    }

    this.select = (event) => {
      this.q = this.opts.label = event.target.textContent
      this.setValue(event.target.dataset.value)
      this.showSuggestions = false
      this.update()
    }

    this.setValue = (value) => {
      this.root.value = value
      this.root.setAttribute('value', value)
    }
});

riot.tag2('select-country', '<option value="" selected="{!this.opts.country}" onchange="{this.opts.onchange}">{this.label || ⁗Select a country⁗}</option> <option each="{country in this.countries}" riot-value="{country[0]}" selected="{this.opts.country == country[0]}"> {country[1]} </option>', '', '', function(opts) {
    countries = config.COUNTRIES
    this.countries = Object.keys(countries).map(code => [code, countries[code]]).sort((a, b) => a[1] > b[1] ? 1 : -1)
});


riot.tag2('select-language', '<option value=""> - Language - </option> <option if="{this.opts.label}" value="" selected="{!this.opts.language}">{this.opts.label || ⁗Select a language⁗}</option> <option each="{tuple in this.languageCodes}" riot-value="{tuple[0]}" selected="{this.opts.language == tuple[0]}"> {tuple[1]} </option>', '', '', function(opts) {
    this.languageCodes = [
      ['en', "English"],
      ['fr', "French"],
      ['de', "German"],
      ['it', "Italian"],
      ['pt', "Portuguese"],
      ['ru', "Russian"],
      ['es', "Spanish"],
    ]
});


riot.tag2('stack-navigation', '<header class="row"> <nav> </nav> <h1 if="{this.opts.title}">{this.opts.title}</h1> </header> <section ref="container"> <yield></yield> </section>', 'stack-navigation,[data-is="stack-navigation"]{ display: block; width: 100%; height: 100%; white-space: nowrap; position: relative; overflow-x: hidden; } stack-navigation > header,[data-is="stack-navigation"] > header{ height: 2rem; margin-bottom: .5rem; padding-bottom: .5rem; border-bottom: 1px solid #eee; } stack-navigation > header > h1,[data-is="stack-navigation"] > header > h1{ margin: 0 auto; font-size: 1.2rem; } stack-navigation [ref=container],[data-is="stack-navigation"] [ref=container]{ width: 100%; height: 100%; min-height: 2rem; } stack-navigation [ref=container] > *,[data-is="stack-navigation"] [ref=container] > *{ position: absolute; left: 110%; display: block; width: 100%; height: 100%; background-color: #fff; transition: .3s left; } stack-navigation .previous,[data-is="stack-navigation"] .previous{ transition: .4s; left: -20%; } stack-navigation .active,[data-is="stack-navigation"] .active{ left: 0; transition: .3s left; }', '', function(opts) {

    this.on('*', () => {
      this.ctx = Object.assign({}, this.opts.ctx, { route: this.opts.route })
      const layerName = this.ctx.route
      const activeTag = Object.values(this.tags).find(t => t.opts.layer === layerName)
      activeTag && activeTag.root.classList.add('active')
    })

});

riot.tag2('tag', '<icon figure="{this.opts.type}"></icon> <div> <a href="{location.pathname}?{this.opts.type}={this.opts.riotValue}|{this.opts.label}" title="Filter with {this.opts.label} as {this.opts.type}" class="{this.opts.type}"> {this.opts.label} </a> <span if="{this.opts.externalUrl}"> <br> Orphanet: <a href="{this.opts.externalUrl}" title="{this.opts.externalTitle}" target="_blank" class="{this.opts.type}">{this.opts.riotValue}</a> </span> <span if="{this.opts.detailsLink}"> <a href="{getUrl(this.opts.detailsLink)}" class="disease">Database card</a> </span> </div>', 'tag,[data-is="tag"]{ display: inline-flex; align-items: center; } tag icon,[data-is="tag"] icon{ margin-right: 0.4rem; } tag > div > .disease,[data-is="tag"] > div > .disease{ font-weight: 500; } tag span a,[data-is="tag"] span a{ border-bottom: 1px solid var(--disease) !important; } tag span a:hover,[data-is="tag"] span a:hover{ border-bottom: none !important; } tag span + span,[data-is="tag"] span + span{ margin-left: 0.2rem; padding-left: 0.4rem; border-left: 1px solid var(--border-color); }', '', function(opts) {
});
riot.tag2('toast', '<div class="{this.type}"> <span> {this.message} </span> <button if="{this.type==\'error\'}" onclick="{this.close}" class="plain"> <icon figure="close-white"></icon> </button> </div>', 'toast,[data-is="toast"]{ position: fixed; left: 50%; bottom: -10rem; min-width: 15rem; width: auto; min-height: 2.8rem; height: auto; margin-left: -7.5rem; overflow: hidden; z-index: 9; transition: bottom .3s; border-radius: .4rem; } toast.visible,[data-is="toast"].visible{ bottom:0; } toast div,[data-is="toast"] div{ display: flex; padding: .8rem; color: #fff; background: #4646AA; border-radius: .4rem; } toast div:before,[data-is="toast"] div:before{ content: \'\'; display: block; width: 1.2rem; height: 1.2rem; margin-right: 0.4rem; background-image: url(/src/images/icons/message-info-white.svg); } toast span,[data-is="toast"] span{ width: calc(100% - 2.8rem); } toast .success,[data-is="toast"] .success{ background: #0F8246; } toast .success:before,[data-is="toast"] .success:before{ background-image: url(/src/images/icons/message-valid-white.svg); } toast .warning,[data-is="toast"] .warning{ background: #D22D00; } toast .warning:before,[data-is="toast"] .warning:before{ background-image: url(/src/images/icons/message-caution-white.svg); } toast .error,[data-is="toast"] .error{ background: #D20019; } toast .error:before,[data-is="toast"] .error:before{ background-image: url(/src/images/icons/message-alert-white.svg); } toast button,[data-is="toast"] button{ width: auto; height: auto; padding-right: 0; }', '', function(opts) {
    this.type = this.opts.type || 'default'
    this.duration = 5000
    this.message = this.opts.message

    this.show = this.root.show = (message, type) => {
      this.type = type
      this.message = message
      this.root.classList.add('visible')
      if (this.type !== 'error') this.autoClose()
      this.update()
    }

    this.autoClose = () => {
      this._latestTimeOut && clearTimeout(this._latestTimeOut)
      this._latestTimeOut = setTimeout(() => {
        this.root.classList.remove('visible')
        this.update()
      }, this.duration)
    }

    this.close = () => {
      this.root.classList.remove('visible')
      this.update()
    }
});

riot.tag2('tooltip', '<a class="tooltip" onmouseover="{this.showToolTip}" onmouseleave="{this.hideToolTip}" href="{this.link}" target="_blank"><icon figure="info" label="{this.description}"></icon></a>', 'tooltip .tooltip icon,[data-is="tooltip"] .tooltip icon{ vertical-align: text-top; } tooltip .tooltip:hover,[data-is="tooltip"] .tooltip:hover{ background-color: transparent; color: inherit; } tooltip .tooltip icon,[data-is="tooltip"] .tooltip icon{ position: relative; } tooltip .tooltip icon span,[data-is="tooltip"] .tooltip icon span{ display: none; position: absolute; width: 15rem; border-radius: .5rem; padding: .5rem; left: 100%; color: var(--secondary-background-color); background-color: var(--text-color); }', '', function(opts) {
        this.link = ""
        this.description = ""

        this.on("mount", () => {
            this.link = this.opts.link
            this.description = this.opts.description
            this.update()
        })

        this.showToolTip = () => {
            const tooltipDescription = document.querySelector(".tooltip icon span")
            tooltipDescription.style.display = "initial"
        }

        this.hideToolTip = () => {
            const tooltipDescription = document.querySelector(".tooltip icon span")
            tooltipDescription.style.display = "none"
        }

});
riot.tag2('user-details', '<header class="{row: true, deleted: this.deleted}"> <div> <div class="avatar"> <icon figure="placeholder-person"></icon> </div> </div> <div> <h1>{user.first_name} {user.last_name}<span if="{user.title}"> ({ucFirst(user.title)})</span><add-export person-pk="{user.pk}" label="{this.contactEmail}"></add-export></h1> <p class="keyword" if="{this.user.keywords.length}"> <icon figure="keyword"></icon> <a each="{keyword in this.user.keywords}" href="{getUrl(location.pathname, {keyword: keyword.pk + ⁗|⁗ + keyword.label})}" class="keyword"> {keyword.label} </a> </p> <div class="row"> <div> <ul> <li if="{user.country}"><icon figure="address"></icon> {getCountry(user.country).label}</li> <li if="{user.email}"><icon figure="email"></icon> <a target="_blank" href="mailto:{user.label}{encodeURIComponent(\'<\' + user.email + \'>\')}">{user.email}</a></li> <li if="{user.phone}"><icon figure="landline"></icon> {user.phone}</li> <li if="{user.cell_phone}"><icon figure="phone"></icon> {user.cell_phone}</li> <li if="{user.languages.length}"><icon figure="language"></icon>{this.languages.join(\', \')}</li> </ul> </div> <div> <div if="{this.user.diseases.length}"> <h3 if="{this.user.diseases.length == 1}">Is an expert on this disease:</h3> <h3 if="{this.user.diseases.length > 1}">Is an expert on these diseases:</h3> <tag each="{disease in this.user.diseases}" type="disease" label="{disease.name}" external-url="https://www.orpha.net/consor/cgi-bin/OC_Exp.php?Expert={disease.pk}" external-title="View details on Orphanet" riot-value="{disease.pk}" details-link="/disease/{disease.pk}"></tag> </div> <div if="{this.user.groupings && this.user.groupings.length}" class="grouping"> <grouping-tag each="{grouping in this.user.groupings}" riot-value="{grouping}"></grouping-tag> </div> </div> </div> </div> </header> <section if="{this.unarchivedRoles.length}"> <div> <h2>Organisations</h2> <role-card each="{role, index in this.unarchivedRoles}" role="{role}"></role-card> </div> </section> <section class="property-group" if="{this.groupsCount}"> <div> <h2 class="group">Groups</h2> <div each="{role in this.user.roles}" if="{role.activeGroups.length || role.inactiveGroups.length}" class="item"> <h3>{role.organisation.label}</h3> <div class="active" if="{role.activeGroups.length}"> Active: <p each="{group in role.activeGroups}"> <a href="/?group={group.group.pk}|{group.group.label}" title="{group.group.description}">{group.group.label}</a><span if="{group.role}"> {group.role}</span> </p> </div> <div class="inactive" if="{role.inactiveGroups.length}"> Inactive: <p each="{group in role.inactiveGroups}"> <a href="/?group={group.group.pk}|{group.group.label}" title="{group.group.description}">{group.group.label}</a><span if="{group.role}"> {group.role}</span> </p> </div> <div> </div> </section> <section class="events" if="{this.events.length}"> <div> <h2 class="event">Events</h2> <expandable-list items="{this.events}"> <yield> <div each="{event in this.opts.items.slice(0, this.slice)}" class="item"> <a href="/?event={event.pk}|{event.label}" title="{event.label}"> {event.label} </a> <span if="{event.role}"> {event.role} ({event.organisation})</span> </div> </yield> </expandable-list> </div> </section> <section if="{this.user.attachments.length}"> <div> <h2>Documents</h2> <div each="{attachment in this.user.attachments}"> <attachment-link contact-id="{this.user.pk}" attachment="{attachment}"></attachment-link> </div> </div> </section> <section if="{this.user.comment}"> <div> <h2>Comments</h2> <p><raw html="{this.user.comment.replace(/\\n/g, \'<br>\')}"></raw></p> </div> </section> <section if="{this.opts.diffs}"> <diffs diffs="{this.opts.diffs}"></diffs> </section> <section class="toolbar"> <small> id: {this.user.pk} <span if="{this.user.filemaker_pk}">(previously {this.user.filemaker_pk})</span> <p> Created on {trans(new Date(this.user.created_at))} <virtual if="{this.user.created_by}"> by <a href="/person/{this.user.created_by.pk}"> {this.user.created_by.first_name} {this.user.created_by.last_name} </a> </virtual> </p> <p> Modified on {trans(new Date(this.user.modified_at))} <virtual if="{this.user.modified_by}"> by <a href="/person/{this.user.modified_by.pk}"> {this.user.modified_by.first_name} {this.user.modified_by.last_name} </a> </virtual> </p> <a href="{getUrl(\'/person/\' + this.user.pk + \'/history\')}">Blame who messed with this</a> </small> <a href="/person/{this.user.pk}/edit/{location.search}" class="button"><icon figure="edit" embedded></icon> Edit this user</a> </section> <link rel="stylesheet" href="/src/css/details.css">', 'user-details > h2,[data-is="user-details"] > h2{ margin: .2rem 0; color: #ccc; font-size: 1rem; } user-details > header,[data-is="user-details"] > header{ padding: 1rem 0 0; } user-details header h1,[data-is="user-details"] header h1{ margin: .3rem 0; font-family: inherit; font-size: 1.5rem; font-weight: 300; } user-details > header > button,[data-is="user-details"] > header > button{ float: right; } user-details .keyword,[data-is="user-details"] .keyword{ display: inline-flex; flex-wrap: wrap; } user-details .keyword > icon,[data-is="user-details"] .keyword > icon{ margin-right: 0.4rem; } user-details .disease icon,[data-is="user-details"] .disease icon{ height: .8rem; } user-details .events a,[data-is="user-details"] .events a{ color: var(--primary-color) !important; } user-details .events p,[data-is="user-details"] .events p{ display: flex; flex-wrap: wrap; } user-details .events p > span,[data-is="user-details"] .events p > span{ flex-basis: 50%; padding-right: 0.8rem; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; } user-details.details header>div>.row,[data-is="user-details"].details header>div>.row{ margin-top: 0; margin-bottom: 2rem; } user-details .keyword > a:not(:last-of-type):after,[data-is="user-details"] .keyword > a:not(:last-of-type):after{ content: \', \'; } user-details .deleted *,[data-is="user-details"] .deleted *,user-details .deleted ~ * *,[data-is="user-details"] .deleted ~ * *{ opacity: .8; } user-details .deleted h1,[data-is="user-details"] .deleted h1{ text-decoration: line-through; } user-details role-card:not(:first-of-type),[data-is="user-details"] role-card:not(:first-of-type){ margin-top: 1rem; } user-details role-card:not(:first-of-type):before,[data-is="user-details"] role-card:not(:first-of-type):before{ content: ""; display: block; width: 1.5rem; border: none; border-top: solid 1px var(--border-color); margin: 0 auto .7rem 0; }', 'class="details"', function(opts) {
    this.user = {}
    this.unarchivedRoles = []
    this.deleted = false

    this.on('*', async () => {
      if (this.user.pk === this.opts.user.pk) return
      this.user = this.opts.user
      this.unarchivedRoles = this.user.roles.filter(role => !role.is_archived)
      this.deleted = this.user.keywords.find(k => k.name.toLowerCase() === 'to be deleted')
      this.user.keywords = this.user.keywords.sort((k1, k2) => k1.label.toLowerCase() < k2.label.toLowerCase() ? -1 : 1)
      this.languages = this.user.languages.map(l => trans(l))
      this.groupsCount = this.user.roles.reduce((total, role) => total += role.groups.length, 0)
      this.user.roles = this.user.roles.map(role => ({
        ...role,
        activeGroups: role.groups.filter(g => g.is_active),
        inactiveGroups: role.groups.filter(g => !g.is_active),
      }))
      this.events = this.user.roles.map(role => role.events.map(e => ({
        organisation: role.organisation.label,
        pk: e.event.pk,
        label: e.event.label,
        role: e.role,
        date: e.event.start_date,
      }))).flat().sort((a, b) => a.date < b.date ? 1 : -1)
      const request = await api('/basket', { data: { resources: [{ person: this.user.pk }] } })
      this.contactEmail = request.data.data.length && request.data.data[0].email
      this.update()
    })

});

riot.tag2('user-edit', '<header class="row"> <div> <div class="avatar"> <icon figure="placeholder-person"></icon> </div> </div> <div> <h1 if="{user.pk}">Edit: {user.first_name} {user.last_name}<span if="{user.title}"> ({ucFirst(user.title)})</span></h1> <h1 if="{!user.pk}">Add a user</h1> </div> </header> <form onsubmit="{this.save}" class="resource-edit-form"> <fieldset> <div> <div> <legend>Person information</legend> <div class="row"> <div> <label for="title">Title</label> <select name="title"> <option value=""> --- </option> <option each="{title in this.titles}" riot-value="{title.key}" selected="{user.title==title.key}">{title.value}</option> </select> </div> <div class="w50p"> <label for="first_name">First Name</label> <input name="first_name" riot-value="{user.first_name}" placeholder="First name" required onblur="{this.checkDuplicatesName}"> </div> <div class="w50p"> <label for="last_name">Last Name</label> <input name="last_name" riot-value="{user.last_name}" placeholder="Last name" required onblur="{this.checkDuplicatesName}"> </div> </div> <duplicates href="{this.duplicatesNameUri}" resource="person" current-id="{this.user.pk}"></duplicates> <div class="row"> <div class="w33p"> <label for="country">Country</label> <select data-is="select-country" class="row" country="{user.country}" name="country" onchange="{this.changeCountry}"></select> </div> <div class="w33p"> <label for>Primary language</label> <select data-is="select-language" name="languages[]" ref="primary" language="{user.languages[0]}"></select> </div> <div class="w33p"> <label for>Secondary language</label> <select data-is="select-language" name="languages[]" exclude="{this.refs.primary.name}" language="{user.languages[1]}"></select> </div> </div> </div> </div> </fieldset> <fieldset class="w70p"> <div> <div> <legend>Contact Details</legend> <div class="row"> <div class="w50p"> <label for="cell_phone">Cellphone</label> <input name="cell_phone" riot-value="{this.user.cell_phone}" placeholder="Mobile phone number ({this.dialPrefix || \'with international dialing code\'})"> </div> <div class="w50p"> <label for="phone">Landline</label> <input id="phone" name="phone" riot-value="{this.user.phone}" placeholder="Phone number ({this.dialPrefix || \'with international dialing code\'})"> </div> </div> <label for="email">Email</label> <input name="email" riot-value="{this.user.email}" placeholder="Email" onblur="{this.checkDuplicatesEmail}" type="{\'email\'}"> </div> </div> </fieldset> <duplicates href="{this.duplicatesEmailUri}" exact="error" class="error" label="This email is already used by the contact below. Please use another one before saving." current-id="{this.user.pk}"></duplicates> <fieldset> <div> <div> <details> <summary class="disease"> <legend class="disease">Diseases <small if="{this.diseasesCount}">({this.diseasesCount})</small></legend> Expand </summary> <div> <article class="row disease-list" each="{disease, index in this.user.diseases}" id="disease_{index}"> <select-autocomplete url="/disease/complete?limit=10&q=" name="diseases[]" riot-value="{disease.pk}" label="{disease.label}" placeholder="Name of the disease"></select-autocomplete> <button class="button disease" type="button" onclick="{this.removeDisease}" riot-value="{index}"> <icon figure="delete" embedded></icon> Delete </button> </article> <button class="button disease" type="button" onclick="{this.addDisease}"> <icon figure="add" embedded></icon> Add a disease </button> </div> </details> </div> </div> </fieldset> <fieldset class="keywords"> <div> <div> <details> <summary class="keyword"> <legend class="keyword">Keywords <small if="{this.user.keywords.length}">({this.user.keywords.length})</small></legend> Expand </summary> <div each="{category in this.keywords}"> <h5>{category.kind}</h5> <div class="cols3"> <span each="{kw in category.values}"> <input type="checkbox" name="keywords[]" id="keyword_{kw.pk}" riot-value="{kw.pk}" checked="{this.userKeywordsPks.includes(kw.pk)}" class="keyword"> <label for="keyword_{kw.pk}"> <span>{kw.label}</span> </label> </span> </div> </div> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary> <legend>Organisations <small if="{this.rolesCount}">({this.rolesCount})</small></legend> Expand </summary> <div> <article hidden="{archivedRoles.includes(role.organisation.pk)}" id="role_edit_{index}" subform="roles[]" each="{role, index in this.user.roles}"> <div class="row"> <div class="w50p"> <label for="organisation">Name of the organisation</label> <select-autocomplete url="/contact/complete?resource=organisation&limit=10&q=" class="row" name="organisation" riot-value="{role.organisation && role.organisation.pk}" label="{role.organisation.label}" placeholder="Name of the organisation"></select-autocomplete> </div> <div class="w50p"> <label for="organisation_role">Role inside this organisation</label> <input class="row" id="organisation_role" name="name" placeholder="Role inside this organisation" riot-value="{role.name}"> </div> </div> <div class="row"> <div class="w50p"> <label for="email">Specific email</label> <input id="role_email_{index}" class="row" name="email" pattern="[^@\\s]+@[^@\\s]+\\.[^@\\s]+" placeholder="Specific email with this organisation" riot-value="{role.email}" type="{\'email\'}"> </div> <div class="fx-grow-2"> <label for="phone">Specific phone number</label> <input riot-value="{role.phone}" name="phone" placeholder="Specific phone number with this organisation"> </div> </div> <div class="row justify-between" style="margin-top: .8rem"> <div class="primary-contact"> <input type="checkbox" value="1" id="is_primary_{index}" name="is_primary" checked="{role.is_primary || \'\'}"> <label for="is_primary_{index}">This is a primary contact</label> </div> <div class="primary-contact"> <input type="checkbox" value="1" id="is_default_{index}" name="is_default" index="{index}" checked="{role.is_default || \'\'}"> <label for="is_default_{index}">This is the default organisation</label> </div> <div class="primary-contact"> <input type="checkbox" value="1" id="is_voting_{index}" name="is_voting" index="{index}" checked="{role.is_voting || \'\'}"> <label for="is_voting_{index}">Voting contact</label> </div> </div> <div class="row justify-between" style="margin-top: .8rem"> <div class="primary-contact" hidden> <input type="checkbox" value="1" id="is_archived_{index}" class="archived-checkbox" name="is_archived" index="{index}" checked="{role.is_archived || \'\'}"> <label for="is_archived_{index}">Archived</label> </div> <div class="align-self-end"> <button class="button ml-auto" type="button" onclick="{this.removeRole}" riot-value="{index}" organisation-pk="{role.organisation.pk}"> <icon figure="delete" embedded></icon> Delete </button> </div> </div> </article> <button class="button" type="button" onclick="{this.addRole}"> <icon figure="add" embedded></icon> Link to an organisation </button> </div> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary class="group"> <legend class="group">Groups <small if="{this.groupsCount}">({this.groupsCount})</small> </legend> Expand </summary> <div each="{role in this.validRoles}"> <h5>{role.organisation.label}</h5> <article id="group_{role.organisation.pk}_{index}" class="groups row" subform="groups[]" each="{userGroup, index in role.groups}"> <input type="hidden" name="organisation" riot-value="{role.organisation.pk}"> <select name="group" required> <option value=""> - Group name - </option> <optgroup each="{category in this.groups}" label="{category.kind}"> <option each="{group in category.values}" riot-value="{group.pk}" selected="{userGroup.group?.pk == group.pk}">{group.label}</option> </optgroup> </select> as <select name="role"> <option value=""> - Role - </option> <option each="{role in this.groupRoles}" riot-value="{role}" selected="{userGroup.role == role}">{role} </option> </select> <input type="checkbox" id="group-{role.organisation.pk}-{index}" name="is_active" value="1" checked="{!userGroup.group ||                   userGroup.is_active}" class="group group_organisation_{role.organisation.pk}_active"> <label class="row" for="group-{role.organisation.pk}-{index}">Still active</label> <button class="button group" type="button" onclick="{this.removeGroup}" riot-value="{index}" organisation="{role.organisation.pk}"> <icon figure="delete" embedded></icon> Delete </button> </article> <button class="button group" type="button" onclick="{this.addGroup}" organisation="{role.organisation.pk}" if="{Number(role.organisation.pk)}"> <icon figure="add" embedded></icon> Add a group </button> </div> <p if="{!this.user.pk}">You must first save this contact before assigning groups.</p> <p if="{!this.validRoles.length}">You must first assign an organisation to this user and save it.</p> <button if="{!this.user.pk || !this.validRoles.length}" class="button" name="save-and-continue-btn"> <icon figure="save-small" embedded></icon> Save and continue </button> </details> </div> </div> </fieldset> <fieldset> <div> <div> <details> <summary class="event"> <legend class="event">Events <small if="{this.eventsCount}">({this.eventsCount})</small> </legend> Expand </summary> <div each="{role in this.validRoles}"> <h5>{role.organisation.label}</h5> <article id="event_{role.organisation.pk}_{index}" class="events row" subform="events[]" each="{event, index in role.events}"> <input type="hidden" name="organisation" riot-value="{role.organisation.pk}"> <div class="row align-end"> <div class="w50p"> <label for="event_name">Name of the event</label> <select-autocomplete url="/event/complete?resource=person&limit=10&q=" id="event_name" name="event" riot-value="{event.event && event.event.pk}" label="{event.event && event.event.label}" placeholder="Name of the event"></select-autocomplete> </div> <div class="w50p"> <label for="event_role">Came as the following role</label> <input class="row" id="event_role" name="role" list="event_role_list" riot-value="{event.role}"> <datalist id="event_role_list"> <option each="{role in this.eventRoles}" riot-value="{role}"> </datalist> </div> <button class="button" type="button" onclick="{this.removeEvent}" organisation="{role.organisation.pk}" riot-value="{index}"> <icon figure="delete" embedded></icon> Delete </button> </div> </article> <button class="button event" type="button" onclick="{this.addEvent}" organisation="{role.organisation.pk}" if="{Number(role.organisation.pk)}"> <icon figure="add" embedded></icon> Add an event </button> </div> <p if="{!this.user.pk}">You must first save this contact before assigning events.</p> <p if="{!this.validRoles.length}">You must first assign an organisation to this user and save it.</p> <button if="{!this.user.pk || !this.validRoles.length}" class="button" name="save-and-continue-btn"> <icon figure="save-small" embedded></icon> Save and continue </button> </details> </div> </div> </fieldset> <contact-attachment-edit contact="{this.user}"></contact-attachment-edit> <fieldset> <div> <div> <details open> <summary> <legend>Comment</legend> Expand </summary> <div> <label for="comment">Comment</label> <textarea name="comment" placeholder="Extra information about this contact">{this.user.comment}</textarea> </div> </details> </div> </div> </fieldset> <section class="toolbar"> <div class="secondary"> <button class="button" type="button" class="plain" if="{this.currentUser.can(\'delete\')}" onclick="{this.delete}"> <icon figure="delete-small" embedded></icon> Delete</button> <button class="button" type="button" onclick="{()=> history.back()}"> <icon figure="close-small" embedded></icon> Cancel</button> <button class="button"> <icon figure="save-small" embedded></icon> Save</button> </div> </section> </form> <link rel="stylesheet" href="/src/css/details.css">', 'user-edit [for=event_role]+[name=role],[data-is="user-edit"] [for=event_role]+[name=role]{ width: 100%; } user-edit .keywords span,[data-is="user-edit"] .keywords span{ display: inline-block; }', 'class="details"', function(opts) {
    this.currentUser = window.user
    this.dbUser = "{}"
    this.user = {}
    this.deleted = false
    this.duplicatesNameUri = ''
    this.duplicatesEmailUri = ''
    this.validRoles = []
    this.diseasesCount = 0
    this.groupsCount = 0
    this.eventsCount = 0
    this.rolesCount = 0
    this.archivedRoles = []

    this.on('*', () => {
      if (!Object.keys(this.user).length) {
        this.user = this.opts.user
      }
      this.dbUser = JSON.stringify(this.user)
      this.diseasesCount = document.getElementsByClassName("disease-list").length
      this.groupsCount = document.getElementsByClassName("groups").length
      this.eventsCount = document.getElementsByClassName("events").length
      var archivedCheckboxes = document.getElementsByClassName("archived-checkbox")
      this.rolesCount = Array.prototype.reduce.call(
        archivedCheckboxes,
        (rolesCount, archivedCheckbox) => archivedCheckbox.checked ? rolesCount : rolesCount + 1,
        0
      )
      this.validRoles = this.user.roles.filter(role => role.organisation?.pk.trim())
      this.userKeywordsPks = this.user.keywords.map(k => k.pk)
    })

    this.on('mount', () => {
      const country = getCountry(this.user.country)
      this.dialPrefix = country && `+${country.prefix}`
      this.archivedRoles = this.user.roles.filter(role => role.is_archived).map(role => role.organisation.pk)
      this.update()
    })

    this.changeCountry = (event) => {
      const country = getCountry(event.currentTarget.value)
      this.dialPrefix = country && `+${country.prefix}`
      this.update()
    }

    this.checkDuplicatesName = (event) => {
      const { first_name, last_name } = serializeForm(event.target.closest('form'))
      const full_name = first_name + (last_name ? ` ${last_name}` : '')
      this.duplicatesNameUri = `/complete?q=${full_name}&limit=5`
    }

    this.checkDuplicatesEmail = (event) => {
      const { email } = serializeForm(event.target.closest('form'))
      this.duplicatesEmailUri = email && `/complete?q=${email}&limit=5`
    }

    this.keywords = classifyByKind(config.KEYWORDS.filter(k => k.for_person))
    this.groups = classifyByKind(config.GROUPS)
    this.groupRoles = config.PERSON_TO_GROUP_ROLES
    this.eventRoles = config.EVENT_ROLES
    this.titles = config.TITLES

    const RoleType = {
      name: "",
      email: "",
      phone: "",
      groups: [],
      is_default: false,
      is_primary: false,
      organisation: {},
      person: {},
    }

    this.save = async (event) => {
      event.preventDefault()
      const readonlyFields = ["pk", "attachment", "uri", "resource", "label", "created_at", "modified_at", "created_by", "modified_by", "filemaker_pk", "scopes", "groupings", "groups", "events"]

      const dbUserObj = JSON.parse(this.dbUser)
      const comparableUser = {
        ...dbUserObj,
        diseases: sort((dbUserObj.diseases || []).map(o => Number(o.pk))),
        keywords: sort((dbUserObj.keywords || []).map(o => Number(o.pk))),
        events: (dbUserObj.events || []).map(o => {
          return new Event(Object.assign(o, { event: o.event.pk }))
        }),
        roles: (dbUserObj.roles || []).map(o => {
          return new Role(Object.assign(o, {
            person: this.user.pk,
            organisation: o.organisation.pk
          }))
        })
      }

      const formData = serializeForm(event.target, true)
      const newUser = {
        ...formData,
        diseases: sort(formData.diseases),
        languages: formData.languages.filter(l=>l),
        keywords: sort(formData.keywords).filter(k=>k),
        roles: (formData.roles || []).map(o => {
          return new Role(Object.assign(o, {
            person: this.user,
            groups: (formData.groups || []).filter(g => g.organisation == o.organisation),
            events: (formData.events || []).filter(g => g.organisation == o.organisation),
          }))
        })
      }

      const data = diffing(comparableUser, newUser, readonlyFields)
      const dataDiff = !!Object.keys(data).length

      const response = dataDiff ? (this.user.pk
        ? apiResponse(await api(`/person/${this.user.pk}`, { method: 'patch', data }))
        : apiResponse(await api(`/person`, { method: 'post', data }))) : null
      if (!dataDiff || response?.ok) {
        if(response?.ok) {
          const user = response.data

        await document.querySelector('contact-attachment-edit')._tag.save(user)

        if (event.submitter.name === 'save-and-continue-btn') {
          this.user =  user
          this.update()
        }
        else
            page(`/person/${user.pk}${location.search}`)
        }

        notify(`Contact was saved successfully`, 'success')
      }
    }

    this.addDisease = (event) => {

      this.user.diseases.push({ pk: ' ', name: '' })
      this.update()
    }

    this.removeDisease = (event) => {
      const disease = document.getElementById(`disease_${event.currentTarget.value}`)
      if (disease) {
        disease.remove()
      }
      this.update()
    }

    this.addRole = () => {
      this.user.roles.push({ organisation: { pk: ' ' }, groups: [], events: [] })
      this.update()
    }

    this.removeRole = (event) => {
      if (!window.confirm('Are you sure you want to remove this role ?'))
        return
      const organisationPk = event.currentTarget.getAttribute('organisation-pk')
      if (organisationPk == ' ') {
        this.user.roles.splice(event.currentTarget.value, 1)
      } else {

        document.getElementById(`is_archived_${event.currentTarget.value}`).checked = true
        document.getElementById(`is_default_${event.currentTarget.value}`).checked = false
        document.getElementById(`role_email_${event.currentTarget.value}`).value = null
        this.archivedRoles.push(organisationPk)

        groups_active = document.getElementsByClassName(`group_organisation_${organisationPk}_active`)
        for (const group_active of groups_active) {
          group_active.checked = false
        }
      }
      this.update()
    }

    this.addGroup = (event) => {
      const organisationPk = event.currentTarget.getAttribute('organisation')
      const role = this.user.roles.find(r => r.organisation.pk == organisationPk)
      if(!role.groups) role.groups = []
      role.groups.push({})
      this.update()
    }

    this.removeGroup = (event) => {
      const group = document.getElementById(`group_${event.currentTarget.getAttribute('organisation')}_${event.currentTarget.value}`)
      if (group) {
        group.remove()
      }
      this.update()
    }

    this.addEvent = (event) => {
      const organisationPk = event.currentTarget.getAttribute('organisation')
      const role = this.user.roles.find(r => r.organisation.pk == organisationPk)
      if(!role.events) role.events = []
      role.events.push({})
      this.update()
    }

    this.removeEvent = (event) => {
      const roleEvent = document.getElementById(`event_${event.currentTarget.getAttribute('organisation')}_${event.currentTarget.value}`)
      if (roleEvent) {
        roleEvent.remove()
      }
      this.update()
    }

    this.delete = async (event) => {
      if (!confirm('You are about to permanently delete this contact. Are your sure?')) return
      const response = apiResponse(await api(`/person/${this.user.pk}`, { method: 'delete' }))
      if (response.ok) {
        notify(`The contact "${this.user.first_name} ${this.user.last_name}" was deleted.`, 'success')
        page(`/${location.search}`)
      }
      else {
        notify(`Could not delete contact: ${response.data.error}`, 'error')
      }
    }

});
